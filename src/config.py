### Configuration file for yso.py
import StringIO, os, numpy as np, math
import pandas as pd

rewrite = False #setting this param to True forces each calculation to be done even if it has already been done, rewriting old calculations in a given value/color.
#This is useful in cases where you switch the data out interactively.

abs_directory = os.path.abspath(os.path.dirname(__file__))+"/" #for absolute path directories.

#Spectral type configuration.
spectral_types         = ["O", "B", "A", "F", "G", "K", "M", "L", "T", "Y"]
snap_to_spt            = [y[0]+y[1] for y in zip([x/2 for x in range(0,20)], [0,0.5]*20)] #integers and half-integers from 0 to 9.5, inclusive.
snap_threshold         = 0.001

def SpT_to_num(SpT):
  """Helper function for conversion of SpT to numerical values."""
  def _is_num_part(char):
    if char in "0123456789.": return True
    else: return False
  if isinstance(SpT, str):
    return float(str(spectral_types.index(SpT[0])+1)+filter(_is_num_part, SpT[1:]))
  else: return np.NaN

def num_to_SpT(num):
  """Helper function for conversion of numerical values to SpT."""
  snapped_num = num % 10
  for snap_target in snap_to_spt:
    if -snap_threshold < snapped_num - snap_target < snap_threshold:
      snapped_num = snap_target
  if isinstance(SpT, str):
    return "nan"
  else:
    if np.isfinite(num):
      return spectral_types[int(num/10) - 1] + str(snapped_num)
    else: return "nan"

vo = {}
# VOTable parsing env_vars. To modify, change the assigned value (not the key)
# This is specific to the implementation of the reading function for defaults["file"]
vo["RESOURCE"]         = "RESOURCE"
vo["TABLE"]            = "TABLE"
vo["ID"]               = "ID"
vo["DESCRIPTION"]      = "DESCRIPTION"
vo["FIELD"]            = "FIELD"
vo["name"]             = "name"
vo["datatype"]         = "datatype"
vo["DATA"]             = "DATA"
vo["TABLEDATA"]        = "TABLEDATA"

defaults = {}
# Default values for a variety of YSO calculations.
defaults["roundto"]    = 2
defaults["table"]      = "table6"
defaults["file"]       = abs_directory+"pecaut_mamajek.vot"
defaults["extinction"] = abs_directory+"extinction.mathis.dat"
defaults["filters"]    = abs_directory+"new_reddening"
defaults["lt_data"]    = abs_directory+"low_temp_extra_data"
defaults["extra_file"] = abs_directory+"EEM_dwarf_UBVIJHK_colors_Teff.dat"
#IMPORTANT: If you replace extra_file OR file, you must replace the following definitions dictionary.
#It is only used in the case of multi_interpol_scheme = "fallback"
#In which case interpolators are generated from the custom table in extra_file
#Programmatically and used.
#The keys of this dictionary are the defaults["file"] columns.
#The values corresponding are the equivalent defaults["extra_file"] columns.
#This is to potentially deal with discrepancies in naming.
column_dictionary = {"SpT": "#SpT",
                     "Teff": "Teff",
                     "B-V": "B-V",
                     "V-Ic": "V-Ic",
                     "V-Ks": "V-Ks",
                     "J-H": "J-H",
                     "H-Ks": "H-K",
                     "Ks-W1": "Ks-W1",
                     #"W1-W2": "W1-W2",
                     "BCV": "BCv"}

#Loading sequence for EEM_dwarf_UBVIJHK_colors_Teff:
data_dwarf = pd.read_csv(defaults["extra_file"], delim_whitespace=True, header=20, nrows=107)
#Be careful about SpT.
data_dwarf[column_dictionary["SpT"]] = map(SpT_to_num, data_dwarf[column_dictionary["SpT"]])
#Do a general loop in order to coerce everything to numerics.

def to_numeric_shim(column):
  column = column.tolist()
  for idx in range(len(column)):
    try:
      column[idx] = float(column[idx])
    except ValueError:
      column[idx] = np.NaN
  return column

for col_name in column_dictionary:
  #data_dwarf[column_dictionary[col_name]] = pd.to_numeric(data_dwarf[column_dictionary[col_name]], errors="coerce")
  ##Turn the strings from reading in a CSV file to floats and NaN's.
  data_dwarf[column_dictionary[col_name]] = to_numeric_shim(data_dwarf[column_dictionary[col_name]])
##Add the Ks-W2 data manually by adding Ks-W1 and W1-W2.
#data_dwarf["Ks-W2"] = data_dwarf["Ks-W1"] + data_dwarf["W1-W2"]
##Unfortunately this doesn't seem to have any valid values.
#TODO: Keep the interpolation in W1-W2, somehow. This would give us significant data on LT stars.

#defaults["isochrones"] = "BCAH98_iso.3_extend"
#defaults["i_comments"] = "--------------------------------------------------------------------------------"
#defaults["i_front"]    = "log t (yr) =  "

#switch between the "append" format and the "fallback" format.
#the append format uses defaults["lt_data"] to augment defaults["file"].
#both are loaded in the interpolator class.
#the fallback format uses defaults["extra_file"] when outside the bounds of defaults["file"].

#multi_interpol_scheme = "append"
multi_interpol_scheme = "fallback"

#The following only applies if multi_interpol_scheme = "append"
interpol              = {}
#interpol["SpT->Teff"]  = "fit"
interpol["SpT->Teff"] = "data"
#To turn on the data-fit, comment the "fit" and use the "data"
interpol["SpT->BCV"]  = "data"
interpol["Teff->BCV"] = "data"
#To turn off the data-fit for the BCV's, comment out the above lines.

def spt_errors(x):
  if x < 50.1: return 2.0
  elif x < 69.9: return 1.0
  elif np.isnan(x): return np.NaN
  else: return 0.5

errors = {}
errors["spt"]          = spt_errors
errors["phot"]         = lambda x: 0.01+0*x #flat
errors["dist"]         = lambda x: 0.1*x #10% error

filters = {}
# Still more defaults (this time specific to filters):
filters["Av_scale"]    = 3.55
filters["cols"]        = (2,3,4)
filters["skip_lines"]  = 14
filters["x_scale"]     = 10000.0

stats = {}
# Weights for calculating weighted average values of parameters.
stats["Ic"]            = 1.0
stats["Rc"]            = 1.0
stats["V"]             = 0.9
stats["J"]             = 0.7
stats["H"]             = 0.3
stats["Ks"]            = 0.2
stats["default"]       = 0.2

import datetime
iso = {}
iso["MAXAGE"]          = 100000000.0 #if earlier than F0, allow post-MS solutions
iso["PLEVELS"]         = [5.0, 10.0, 68.0, 95.0, 99.7]
curr_dt                = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
iso["resource"]        = os.path.dirname(__file__)+"/dat/"+curr_dt+"/bayes/resources/"
iso["filename"]        = os.path.dirname(__file__)+"/dat/"+curr_dt+"/bayes/index.html"
iso["increment"]       = 3
iso["grid"]            = False

def load_BCAH98():
  """Assumes BCAH98_iso.3_extend is in the local directory.
  Currently, BCAH98 tracks correspond to 994 points in isochrone space."""
  with open("BCAH98_iso.3_extend", "r") as f:
    isotxt = f.read()
    isotxt = isotxt.split("log t (yr) =  ")[1:]
    points = [] #Age (yr), Mass (Msun), logT, logL
    for idx, power_age in enumerate(isotxt):
      curr_age = np.power(10.0, float(power_age.split("\n")[0]))
      raw_points = np.genfromtxt(StringIO.StringIO(isotxt[idx].replace("--------------------------------------------------------------------------------", "#")), usecols=(0,1,3), skiprows=3).tolist()
      for point in raw_points:
        points.append([curr_age, point[0], np.log10(point[1]), point[2]]) #Get in logT format
  return points

def load_dartmouth():
  """Assumes Dartmouth tracks are in directory ./dartmouth_tracks/
  Currently, Dartmouth tracks correspond to 375,286 points in isochrone space.
  Loading all these points takes on the order of a dozen seconds on a modern PC. (2014)
  TODO: Resample this space as necessary!
  Currently (randomly) selecting only ~1000 points."""
  import random
  directory = "./dartmouth_tracks/"
  points = [] #Age (yr), Mass (Msun), logT, logL
  files = [f for f in os.listdir(directory) if os.path.isfile(directory+f)]
  for filename in files:
    with open(directory+filename, "r") as f:
      isotxt = f.read()
      if len(isotxt) > 0:
        curr_mass = float(isotxt.split("M=")[1].split(" X=")[0])
        raw_points = np.genfromtxt(StringIO.StringIO(isotxt), usecols = (0,1,3)).tolist()
        for point in raw_points:
          if True: #random.random() < 1.0/375.286:
            points.append([point[0], curr_mass, point[1], point[2]])
  return points

loaded_isochrone       = load_dartmouth

def uniform_age_prior(isopoints):
  prior_array = np.empty(len(isopoints))
  for idx, (age, mass, logt, logl) in enumerate(isopoints):
    if age > iso["MAXAGE"]:
      prior_array[idx] = 0.0
    else:
      prior_array[idx] = 1.0
  return prior_array

def chabrier_mass_prior(isopoints):
  prior_array = np.empty(len(isopoints))
  for idx, (age, mass, logt, logl) in enumerate(isopoints):
    if mass < 1:
      prior_array[idx] = 0.093*math.pow(np.e, -((math.log10(mass)-math.log10(0.2))**2)/(2*(0.55**2))) #mass prior, chabrier IMF (2003 review paper)
    else:
      prior_array[idx] = 0.041*math.pow(mass,-1.35) #\pm 0.3 on the exp.
  return prior_array

prior                  = {}
prior["age"]           = uniform_age_prior
prior["mass"]          = chabrier_mass_prior

general                = {}
general["loc"]         = os.path.dirname(__file__)+"/dat/"+curr_dt

header                 = lambda x: "# %s data. Generated by yso.py <http://www.astro.caltech.edu/~cccao/stars/> on %s." % (x, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

av_sed                 = [x/20.0 for x in range(0, 7*20)] #0 to 19.95 in 0.05 increments.
