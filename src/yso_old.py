
  def set_data(self, names, spt, phot):
    """Add (with replacements) data.
    Data loaded into yso_data are:
    (*) Photometry (along with color tables, and None values)
    (*) Spectral Types (one per star)
    Validation is done on a star-by-star basis here, and stars without
    * a valid spectral type or at least one pair of colors corresponding
    * to a valid (possibly aliased) color in the attached interpolator
    * object throw a warning to standard output.
    A benefit of data buckets is that values can be added on a
    * star-by-star basis, or optionally across more advanced queries.
    Names are case-sensitive.
    Colors are case-sensitive, and are parsed depending on the syntax
    * provided.
    (*) If just one value is provided, an error is assumed of a default
        value specified in config.py.
    (*) If a two-tuple is provided, the first value is assumed to be the
        magnitude at that band, and the second value is assumed to be
        the (symmetric) error.
    Spectral types are also case-sensitive:
    (*) If one value is provided, a default config.py error is used.
    (*) If a two-tuple is provided, the first value is assumed to be the
        value of the SpT, and the second value is assumed to be the
        symmetric error about that value.
    Example:
    * taurus_data = yso_data(["AA Tau"], [{"V": (1.23, 0.015), "J": (0.44, 0.042)}], [("K2V", 1.0)])"""
    assert len(names) == len(spt) == len(phot.items()[0])-1, "Data assignment not 1:1."
    assert reduce(lambda y, z: y == z, [len(x) for x in phot]), "Inconsistent photometry lengths."
    headers, phot_bands = zip(*phot.items())
    phot_bands = zip(*phot_bands) #Appropriately ordered by star now.
    for star in zip(names, spt, phot_bands):
      if type(star[1]) not list or not tuple: #check if this conditional is generally true
        data_spt = star[1]
        data_spt_err = config.defaults["spt_err"]
      elif len(star[1]) == 1:
        data_spt = star[1][0]
        data_spt_err = config.defaults["spt_err"]
      elif len(star[1]) == 2:
        data_spt = star[1][0]
        data_spt_err = star[1][1]
      else:
        print "Warning: Spectral type data format incompatible."
      for idx_c, color_phot in enumerate(star[2]):
        if type(color_phot) not list or not tuple:
          data_phot = color_phot
          data_phot_err = config.defaults["phot_err"]
        elif len(color_phot) == 1:
          data_phot = color_phot[0]
          data_phot_err = config.defaults["phot_err"]
        elif len(color_phot) == 2: ###
          data_phot = color_phot[0]
          data_phot_err = color_phot[1]
        else:
          print "Warning: Photometry data format incompatible."
      self.stars[star[0]] = ([data_spt, data_spt_err], [data_phot, data_phot_err])
