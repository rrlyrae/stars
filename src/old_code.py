
  def find_shortest_path(self, graph, start, end, path=[]):
    """From https://www.python.org/doc/essays/graphs/ on 6/23/14"""
    path = path + [start]
    if start == end:
        return path
    if not graph.has_key(start):
        return None
    shortest = None
    for node in graph[start]:
        if node not in path:
            newpath = self.find_shortest_path(graph, node, end, path)
            if newpath:
                if not shortest or len(newpath) < len(shortest):
                    shortest = newpath
    return shortest
  def identify_colors(self, color_things):
    colors = []
    self.graph, self.pos_graph = {},{} # Directed edges hack.
    for f, t in [x[0].split("-") for x in self.table["fields"] if "-" in x[0]]:
      if not self.graph.has_key(f):
        self.graph[f] = [t]
        self.pos_graph[f] = [t]
      else:
        self.graph[f].append(t)
        self.pos_graph[f].append(t)
    for t, f in [x[0].split("-") for x in self.table["fields"] if "-" in x[0]]: #graph is bidirectional
      if not self.graph.has_key(f):
        self.graph[f] = [t]
      else:
        self.graph[f].append(t)
    color_combo_tuples = list(combinations(color_things,2)) # Find all possible color combinations.
    color_combo = [list(x) for x in color_combo_tuples]
    self.load_extinction_law()
    for color_combination in color_combo: #short wavelength first.
      if self.filter_to_lambda(color_combination[0])>self.filter_to_lambda(color_combination[1]):
        color_combination[0],color_combination[1]=color_combination[1],color_combination[0]
    color_combo = sorted(sorted(color_combo, key=lambda elem: self.filter_to_lambda(elem[1])), key=lambda elem: self.filter_to_lambda(elem[0])) #sort the colors by shortest wavelength to longest.
    for color_combination in color_combo:
      path = self.find_shortest_path(self.graph, color_combination[0], color_combination[1])
      gen_path = []
      for idx in range(len(path)-1): #throws a TypeError if color is not in the database.
        if self.pos_graph.has_key(path[idx]) and path[idx+1] in self.pos_graph[path[idx]]: #forward-facing
          gen_path.append((path[idx], path[idx+1], 1))
        elif self.pos_graph.has_key(path[idx+1]) and path[idx] in self.pos_graph[path[idx+1]]:
          gen_path.append((path[idx+1], path[idx], -1))
      colors.append(gen_path)
    return (colors, color_combo_tuples, color_combo)



class yso(object):
  def observed_colors(self):
    """Generate the appropriate summed colors from loaded photometry.
    Returns a Numpy array with the observed colors with the dimensions: Colors x Stars.
    Assumes errors of 1% in each photometric value if errors are not loaded."""
    self.ocolors = np.empty((len(self.colors), len(self.SpT)))
    self.ocolors_err = np.empty((len(self.colors), len(self.SpT)))
    for idx, color_tuple in enumerate(self.color_combo):
      color_sum = self.photometry[color_tuple[0]]-self.photometry[color_tuple[1]]
      self.ocolors[idx] = color_sum
      self.ocolors_err[idx] = np.sqrt(self.phot_err[color_tuple[0]]**2-self.phot_err[color_tuple[1]]**2)
    return self.ocolors
  def intrinsic_colors_manual(self, color_path):
    """Generate an intrinsic color given a color_path. Example: [("B", "V", 1), ("U", "V", -1)]
    Generates intrinsic colors from the interpolation table."""
    color_sum = np.zeros(len(self.SpT))
    for color in color_path:
      color_sum += self.table["interpolation"]["SpT->%s-%s" % (color[0], color[1])]([self.I.SpT_to_num(x) for x in self.SpT])*color[2]
    return color_sum
  def intrinsic_colors(self):
    """Generate the appropriate summed colors as a function of SpT.
    Returns a Numpy array with all the intrinsic colors with the dimensions: Colors x Stars."""
    self.icolors = np.empty((len(self.colors), len(self.SpT)))
    for idx, color_path in enumerate(self.colors):
      self.icolors[idx] = self.intrinsic_colors_manual(color_path)
    return self.icolors
  def color_excess(self):
    """A simple alias for the calculation of the color excess, as observed_colors-intrinsic_colors."""
    return self.observed_colors() - self.intrinsic_colors()
  def load_extinction_law(self, extinction_law="extinction.mathis.dat", filters="new_reddening", Av_scale=3.55, cols=(2,3,4), skip_lines=14, x_scale=10000.0):
    """Load an extinction law and corresponding filter dictionary from two data files.
    Overwrites existing extinction law if exists."""
    a = np.genfromtxt(extinction_law)
    self.dA_gen = self.I.interpolate([x[0] for x in a], [y[1]/Av_scale for y in a])
    reddening_filters = np.genfromtxt(filters, skip_header=skip_lines, usecols=cols, dtype=None)
    self.lambda_eff = dict([[x[0],x[1]/x_scale] for x in reddening_filters if x[0] != "?"]) #generate color to lambda_eff (microns) table
  def extinction_law_pure(self, colors1, colors2):
    """Generates and returns the appropriate delta A/A_v (numpy list) for a given pair of color lists with the loaded extinction law."""
    assert len(colors1) == len(colors2), "Dimensions of color lists provided are inconsistent."
    extinction_law_colors = np.empty(len(colors1))
    for idx, color in enumerate(zip(colors1, colors2)):
      extinction_law_colors[idx] = self.dA_gen(self.lambda_eff[color[0]])-self.dA_gen(self.lambda_eff[color[1]])
    return extinction_law_colors
  def filter_to_lambda(self, filter_abbrev):
    """Helper function to return the effective wavelength of a filter abbreviation, such as "V" or "H"."""
    return self.lambda_eff[filter_abbrev]
  def A_v(self):
    """Calculates and returns the Av values for the loaded colors, extinction law, and other functions."""
    colors1, colors2 = zip(*[[x[0],x[1]] for x in self.color_combo]) #generate the color lists for the extinction law
    self.Av = self.color_excess()*np.reciprocal(self.extinction_law_pure(colors1, colors2)).reshape((len(colors1),1))
    self.Av_err = self.ocolors_err*np.reciprocal(self.extinction_law_pure(colors1, colors2)).reshape((len(colors1),1))
    self.Av = self.Av.clip(min=0) #remove all negative values
    return self.Av
  def A_lambda(self):
    """Calculates and returns the Al values corresponding the the order of the loaded colors.
    Always calculates the Al corresponding to the first of the color pair, sorted by wavelength."""
    self.A_v()
    self.Alambda = np.empty_like(self.Av)
    self.Alambda_err = np.empty_like(self.Av)
    for idx, color in enumerate([x[0] for x in self.color_combo]):
      self.Alambda[idx] = self.Av[idx]*self.dA_gen(self.lambda_eff[color])
      self.Alambda_err[idx] = self.Av_err[idx]*self.dA_gen(self.lambda_eff[color])
    return self.Alambda
  def bcv(self):
    """Returns interpolated bolometric correction values from the loaded tables."""
    self.BCV = self.table["interpolation"]["SpT->BCV"](np.array([self.I.SpT_to_num(x) for x in self.SpT]))
    bcv_p = self.table["interpolation"]["SpT->BCV"](np.array([self.I.SpT_to_num(x) + self.SpT_err[idx] for idx, x in enumerate(self.SpT)]))
    bcv_n = self.table["interpolation"]["SpT->BCV"](np.array([self.I.SpT_to_num(x) - self.SpT_err[idx] for idx, x in enumerate(self.SpT)]))
    #it's possible to run into key errors here..
    self.bcv_err_range = zip(bcv_n, bcv_p)
    return self.BCV
  def bclambda(self):
    """Calculates the modified intrinsic BCLambda corresponding to the color of the row in self.color_combo.
    * TODO (refactor): replace self.graph with modular color pathing"""
    BCL = np.empty((len(self.color_combo), len(self.SpT)))
    self.bcl_err_range = np.empty((len(self.color_combo), len(self.SpT), 2))
    BCV = self.bcv()
    for idx, color in enumerate([x[0] for x in self.color_combo]):
      path = self.find_shortest_path(self.graph, color, "V")
      gen_path = []
      for indx in range(len(path)-1): #throws a TypeError if color is not in the database.
        if self.pos_graph.has_key(path[indx]) and path[indx+1] in self.pos_graph[path[indx]]: #forward-facing
          gen_path.append((path[indx], path[indx+1], 1))
        elif self.pos_graph.has_key(path[indx+1]) and path[indx] in self.pos_graph[path[indx+1]]:
          gen_path.append((path[indx+1], path[indx], -1))
      BCL[idx] = BCV - self.intrinsic_colors_manual(gen_path)
      for indx, x in enumerate(self.intrinsic_colors_manual(gen_path)):
        self.bcl_err_range[idx][indx] = (self.bcv_err_range[idx][0]-x, self.bcv_err_range[idx][1]-x)
    return BCL
  def logl(self):
    """Generates the logl values."""
    self.A_lambda()
    self.app_mag = np.zeros_like(self.Alambda)
    self.app_mag_err = np.zeros_like(self.Alambda)
    for idx, color in enumerate([x[0] for x in self.color_combo]):
      self.app_mag[idx] = self.photometry[color]
      self.app_mag_err[idx] = self.phot_err[color]
    self.LOGL = (4.75+self.Alambda+5*np.log10(np.array(self.dist)/10)-np.array(self.app_mag)-self.bclambda())/2.5
    #currently, for error range, do the most extremes!
    self.logl_err_range = np.array([zip(x[0].tolist(),x[1].tolist()) for x in
    zip((4.75+self.Alambda+self.Alambda_err+5*np.log10(np.array(self.dist+self.dist_err)/10)-np.array(self.app_mag)-np.array(self.app_mag_err)-self.bcl_err_range.swapaxes(0,2)[0].T)/2.5,
    (4.75+self.Alambda-self.Alambda_err+5*np.log10(np.array(self.dist-self.dist_err)/10)-np.array(self.app_mag)+np.array(self.app_mag_err)-self.bcl_err_range.swapaxes(0,2)[1].T)/2.5)])
    self.logl_err = (self.logl_err_range.swapaxes(0,2)[1]-self.logl_err_range.swapaxes(0,2)[0]).T*1.0/2.35
    return self.LOGL
  def r(self):
    """Generates the r values.
    Err not currently implemented."""
    logl = self.logl()
    lsun = 3.8935*math.pow(10.0,26)
    sigma = 5.670400*math.pow(10.0,-8)
    self.rsun_m = 6.95508*math.pow(10.0,8)
    num_prefactor = math.sqrt(lsun/(4*np.pi*sigma))
    teff = self.table["interpolation"]["SpT->Teff"]([self.I.SpT_to_num(x) for x in self.SpT])
    self.R = num_prefactor*np.power(np.power(10.0,logl)*np.reciprocal(np.power(teff,4.0)),0.5)
    return self.R
  def rsun(self):
    """Generates the r/rsun values."""
    return self.r()*(1.0/self.rsun_m)
  def logt(self):
    """Returns logt in a one-dimensional array whose dimension is [Stars]"""
    teff = self.table["interpolation"]["SpT->Teff"]([self.I.SpT_to_num(x) for x in self.SpT])
    teff_p = self.table["interpolation"]["SpT->Teff"](np.array([self.I.SpT_to_num(x) + self.SpT_err[idx] for idx, x in enumerate(self.SpT)]))
    teff_n = self.table["interpolation"]["SpT->Teff"](np.array([self.I.SpT_to_num(x) - self.SpT_err[idx] for idx, x in enumerate(self.SpT)]))
    self.teff_err_range = zip(teff_p, teff_n)
    self.teff_err = np.array([x[1] - x[0] for x in self.teff_err_range])
    #it's possible to run into key errors here..
    return np.log10(teff)
  def get_list(self, data_array, names, round_to=5, stats=False):
    """Supply with data_array and star names.
    Suitable data_array's built in are: self.A_v(), self.logl():
    A.get_list(A.A_v(),names,5)"""
    if stats:
      weights = np.array([config.stats[x[0]] for x in self.color_combo])
      return [[x[0]]+np.round(x[1],round_to).tolist()+[np.round(np.sum(weights*x[1])/np.sum(weights), round_to),np.round(np.std(x[1]), round_to)] for x in zip(names, np.swapaxes(data_array, 0, 1))]
    else: return [[x[0]]+np.round(x[1],round_to).tolist() for x in zip(names, np.swapaxes(data_array, 0, 1))]
  def ascii_list(self, data_array, names, prefix, stats=True):
    """Custom table generator. Accepts a list with table headers, such as that returned by A.get_list()."""
    if stats:
      ifstats = 2
      pre_ascii_table = [["Name"]+["%s(%s)" % (prefix, "-".join(x)) for x in self.color_combo]+["W.Avg"]+["Std"]]+self.get_list(data_array, names, stats=True)
    else:
      ifstats = 0
      pre_ascii_table = [["Name"]+["%s(%s)" % (prefix, "-".join(x)) for x in self.color_combo]]+self.get_list(data_array, names)
    column_width = []
    for column in [[x[n] for x in pre_ascii_table] for n in range(len(self.color_combo)+1+ifstats)]:
      column_width.append(max([len(str(x)) for x in column]))
    ascii_table = ""
    for row in pre_ascii_table:
      curr_row = ""
      for idx, item in enumerate(row):
        curr_row += str(item)+" "*(1+column_width[idx]-len(str(item))) #for padding of 1 char.
      curr_row += "\n"
      ascii_table += curr_row
    return ascii_table
  def __init__(self, filename="pecaut_mamajek.vot", table="table5"):
    self.load_file(filename)
    self.load_table(table)
  def load_data(self, SpT, photometry, dist = None, phot_err = [], dist_err = [], SpT_err = [], alias_colors=(("K", "Ks"))):
    self.colors = []
    self.SpT = SpT
    self.photometry = dict([[x[0], np.array(x[1])] for x in photometry.items()])
    if len(phot_err) > 0:
      self.phot_err = phot_err
    else:
      self.phot_err = dict([[x[0], np.zeros_like(x[1])+0.01] for x in photometry.items()]) #guess a 1% photometric error
      #could add random jitter to this error (monte carlo)
    if dist is not None:
      if len(dist_err) > 0:
        self.dist_err = dist_err
      else:
        self.dist_err = np.array(dist)*0.1 #guess a 10% error in distances, could add random..
      self.dist = dist
    if len(SpT_err) > 0:
      self.SpT_err = SpT_err
    else:
      self.SpT_err = np.ones(len(SpT)) #guess an error of 1 SpT..
      #could add random jitter..
    self.I = interpolator()
    for alias_color in alias_colors:
		self.I.alias_color(alias_color) #offset for inconsistency of cols
  def load_file(self, filename):
    """Load VOTable file with tables and data."""
    self.I.load_file(filename)
  def load_table(self, table):
    """Load the given table from the loaded file."""
    def inn(val):
      def innn(val2):
        return val in val2
      return innn
    self.table = self.I.tables[filter(inn(table), self.I.tables.keys())[0]]
    self.colors, self.color_combo_tuples, self.color_combo = self.identify_colors(self.photometry.keys())

class yso_sed(yso):
  def red_chi_squared(self, observed, model):
    """Calculates the reduced chi-squared statistic for a single star.
    The equation used for calculating the Chi2 statistic is 1/(N-1)*sum((observed - model photometries)/sqrt(photometric error))
    
    :param observed: an ordered list of photometric magnitudes, model: an ordered list of photometric magnitudes corresponding to the values in observed."""
    #return (1.0/(len(observed)-1.0))*np.sum(np.square((observed-model))*np.reciprocal(np.var(observed)))
    return (1.0/(len(observed)-1.0))*np.sum(np.square((observed-model)*np.reciprocal(np.sqrt(0.02))))
    #return (1.0/(len(observed)-1.0))*np.sum(np.square((observed-model)))
  def A_v_sed(self, test=False):
    """Uses SED fitting to calculate a probable Av for each star.
    Returns a one-dimensional numpy array with a single Av value for each star.
    
    First implementation, only works with all colors populated. TODO: support arbitrary configurations of keys.
    *Assumes photometry is already filled with numpy arrays.
    *Assumes self.load_extinction_law() has been called at least once."""
    #photometry_indices = set(self.photometry.keys()) - set("V")
    self.virtual_photometry = {"V": self.photometry["V"]}
    self.Av = np.empty_like(self.photometry["V"])
    self.AvX = np.empty_like(self.photometry["V"])
    for band, color_path in [[x[0][1],x[1]] for x in zip(self.color_combo, self.colors) if x[0][0]=="V"]:
      self.virtual_photometry[band] = self.photometry["V"] - self.intrinsic_colors_manual(color_path)
    for p in range(len(self.photometry["V"])): #length of "V" assumed to be the number of stars.. again, hardcoded.
      xs,vs,ys = zip(*[[self.filter_to_lambda(x[0]), x[1][p], self.photometry[x[0]][p]] for x in self.virtual_photometry.items()])
      self.Av[p], self.AvX[p] = sorted([[Av, self.red_chi_squared(ys, self.dA_gen(xs)*(Av)-Av+vs)] for Av in np.arange(0,20,0.05)], key=lambda chi_sq: chi_sq[1])[0]
    return self.Av



  def get_color_pair(self, first_color, second_color, raw = False):
    """Obtain raw data (photometries) ready for processing by
       * traditional YSO methods.
       Note: previously, YSO methods couldn't handle None values in YSO
       * data, because the entire grid was processed automatically.
       With color pairs, calculations may need to be merged eventually
       * with name-matching for stars.
       Returns a tuple with:
       [0]: name of stars
       [1]: numpy array of first color photometry
       [2]: numpy array of second color photometry
       [3]: numpy array of first color photometric error
       [4]: numpy array of second color photometric error"""
    if raw: return [(star[1][1][0], star[1][1][1]) for star in self.stars.items()]
    return [(star[0], star[1][1][0], star[1][1][1]) for star in self.stars.items()]
    #According to the indices: self.stars[star[0]] = ([data_spt, data_spt_err], [data_phot, data_phot_err])
    #This will return a three-tuple with names, phot, and phot_err.
  def get_spt(self, raw = False):
    """Obtain raw data (spectral types) ready for processing by
       * traditional YSO methods.
       Note: previously, YSO methods couldn't handle None values in YSO
       * data, because the entire grid was processed automatically.
       With color pairs, calculations may need to be merged eventually
       * with name-matching for stars.
       Returns a tuple with:
       [0]: name of stars
       [1]: numpy array of spectral type
       [2]: numpy array of spectral type error"""
    if raw: return [(star[1][0][0], star[1][0][1]) for star in self.stars.items()]
    return [(star[0], star[1][0][0], star[1][0][1]) for star in self.stars.items()]
