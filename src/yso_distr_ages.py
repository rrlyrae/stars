import xml.etree.ElementTree as ET
import re
import scipy as sp
import scipy.interpolate as ip
import matplotlib.pyplot as plt
import numpy as np
from itertools import combinations
from collections import defaultdict as ddict
import math
import config
import StringIO
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import NullFormatter
import urllib, base64, os

class interpolator(object):
  def __init__(self, var_axes=["SpT", "Teff"]):
    """Generate an interpolator object from which certain VOTables can be loaded, with its values interpolated.

    :param var_axes: A list of axes to include"""
    self.tables = {}
    self.var_axes = var_axes #predefined x-axes.
    self.excluded_fields = []
    self.color_table = {}
  def _arrays(self):
    """Helper function for generating arbitrary-length Python arrays."""
    while True: yield []
  def interpolate(self, xax, yax):
    """Helper function for SciPy interpolation."""
    def _and_none(a):
      if a[0] is None or a[1] is None: return False
      else: return True
    assert len(xax) == len(yax), "Lengths of arrays not identical."
    xax, yax = zip(*sorted(filter(_and_none, zip(xax,yax)), key=lambda x: x[0])) #sort, because scipy's splines need to be sorted..
    #NOTE: scipy won't interpolate with multiple x-coords even if they are the same point.
    #this should be fixed in the extinction files..
    spline = ip.InterpolatedUnivariateSpline(xax, yax)
    return spline
  def preload_axis(self, axis):
    """Add an x-axis to the interpolation queue to be included in the table object."""
    self.var_axes.append(axis)
    return self.var_axes
  def exclude_axis(self, axis):
    """Ignore an axis"""
    self.excluded_fields.append(axis)
    return self.excluded_fields
  def alias_color(self, color, other_name):
    """Correct for quirks in certain color conventions in tables."""
    self.color_table[color] = other_name #correct for quirks in tables, such as K := Ks
  def load_file(self, filename="vizier_votable.vot"):
    """Load a VOTable file and interpolation information in tables.
    Field names are assumed to be canonical for a non-prefixed VOTable file. If not, change the config.py global vars."""
    with open(filename, "r") as somefile:
      xmlstring = somefile.read()
      xmlstring = re.sub(' xmlns="[^"]+"\n', '', xmlstring, count=1)
      tree = ET.fromstring(xmlstring)
      for table in tree.find(config.vo["RESOURCE"]).findall(config.vo["TABLE"]):
        parsed_table = {}
        self.tables[table.attrib[config.vo["ID"]]] = parsed_table
        parsed_table["name"] = table.attrib[config.vo["name"]]
        parsed_table["description"] = table.find(config.vo["DESCRIPTION"]).text
        parsed_table["fields"] = []
        for field in table.findall(config.vo["FIELD"]): #Populate field array with appropriate fields and datatype
          #if colors are inconsistent, check divisions along A-B, A and B individually.
          field_name = field.attrib[config.vo["name"]]
          field_name_parts = field_name.split("-")
          for x in range(len(field_name_parts)):
            if field_name_parts[x] in self.color_table:
              field_name_parts[x] = self.color_table[field_name_parts[x]]
          field_name = "-".join(field_name_parts)
          parsed_table["fields"].append((field_name, field.attrib[config.vo["datatype"]]))
        xml_data = table.find(config.vo["DATA"]).find(config.vo["TABLEDATA"])
        parsed_table["data"] = dict(zip(range(len(xml_data[0])), self._arrays()))
        for row_index in range(len(xml_data)): #Populate data dictionary with appropriate data points
          for field_index in range(len(xml_data[row_index])):
            if xml_data[row_index][field_index].text != None:
              #if datatype is "float" or "int", make them float (interpolating, so preserving int isn't a huge concern).
              if parsed_table["fields"][field_index][1] in ("float", "int", "short"): datapoint = float(xml_data[row_index][field_index].text)
              else: #assume it's SpT and assign numerical values..
                datapoint = config.SpT_to_num(xml_data[row_index][field_index].text)
              parsed_table["data"][field_index].append(datapoint) #iterating, so order is preserved.
            else:
              parsed_table["data"][field_index].append(None)
        parsed_table["interpolation"] = {}
        for field_name in self.var_axes:
          curr_axis_index_arr = [parsed_table["fields"].index(x) for x in parsed_table["fields"] if x[0] == field_name]
          if len(curr_axis_index_arr) > 0:
            curr_axis_index = curr_axis_index_arr[0]
            for yaxis in range(0,curr_axis_index) + range(curr_axis_index+1,len(parsed_table["fields"])):
              #print parsed_table["data"][curr_axis_index], parsed_table["data"][yaxis]
              if field_name not in self.excluded_fields:
                parsed_table["interpolation"]["%s->%s" % (field_name, parsed_table["fields"][yaxis][0])] = self.interpolate(parsed_table["data"][curr_axis_index], parsed_table["data"][yaxis])
              #print field_name, parsed_table["fields"][yaxis][0]
    return self.tables

class yso_data(object):
  """Provides a standard API for loading data. Preserves atomicity of
  * data and prevents errors related to impure operations on data."""
  def __init__(self, names = [], spt = [], phot = {}, colors="all"):
    """Initializing yso_data creates an optional data bucket.
    The data does not need to be necessarily passed initially to the
    * object, so long as operations done on the data are all sensical.
    This allows a bucket to be reused, say, in an environment where the
    * datasets may be reused, merged, etc, without preference to the
    * order in which data is loaded.
    The param `colors` can be "all", a string, or a tuple corresponding
    * to keys in phot.

    Note: if initial values are not loaded here, self.set_colors must be
    * called with a list of all the appropriate colors before
    * computation."""
    self.stars = {}
    self.criteria = {}
    if colors == "all": colors = phot.keys()
    if len(names) > 0:
      self.set_data(names, spt, phot)
      self.set_colors(colors)
  def set_data(self, names, values, errors = [], val_name = "phot", color = "value", criteria = u"\xb1"):
    """Passing a list of star names with corresponding values and errors
    * and a valid val_name and optional color will populate the star
    * dictionary with this data.
    Example: {"FPTau": {"spt": {"value": (55.0, 1.0)}, "phot": {"Rs": (2.3, 0.23), "Ic": (0.1, 0.01)}}}"""
    assert len(names) == len(values), "Value mismatch between star names and % values." % val_name
    if val_name == "spt":
      try: values = [float(x) for x in values]
      except ValueError: values = [config.SpT_to_num(x) for x in values]
    if len(errors) == 0:
      #Using default errors from config.py. Perhaps print that we're using defaults?
      errors = [config.errors.get(val_name, lambda x: np.NaN)(x) for x in values]
      #If the values don't have an error default set in config.py, then we (understandably) set the value of the error to NaN.
    for star_name, star_val, star_err in zip(names, values, errors):
      if star_name not in self.stars.keys():
        self.stars[star_name] = {}
      if val_name not in self.stars[star_name].keys():
        self.stars[star_name][val_name] = {}
      self.stars[star_name][val_name][color] = (star_val, star_err)
      self.criteria["%s|%s" % (val_name, color)] = criteria
  def get_data(self, val = "phot", color = "value"):
    temp_data = []
    for star in self.stars:
      temp_data_list = list(self.stars[star].get(val, {}).get(color, (np.NaN, np.NaN)))
      temp_data.append((star, temp_data_list[0], temp_data_list[1]))
      ret_data = zip(*temp_data)
      ret_data[1] = np.array(ret_data[1])
      ret_data[2] = np.array(ret_data[2])
    #if temp_data_list #add dependency structure
    return ret_data #Returns in format (name_list, data_list, err_list)
  def get_value(self, val = "phot"):
    """Returns all colors in a given value."""
    temp_data = []
    for star in self.stars:
      temp_data_list = self.stars[star].get(val, {})
      temp_data.append((star, temp_data_list))
      ret_data = zip(*temp_data)
    return ret_data
  def get_table(self, *vals):
    """Returns the appropriate datatype in an ascii-formatted string.
       Width: # of colors + 1, height: # of stars + 1
       Cannot mix color and non-color inputs.
       TODO: Reimplement sorting of columns by wavelength, without
       * requiring the function from yso_val.
       CLIP ALL COLS WITH COMPLETELY NAN, but not rows."""
    if len(vals) == 0: vals == ("phot")
    def _sign(number, all_pos):
      """Helper function to standardize positive and negative signs."""
      if not all_pos: return " "*int(number >= 0)+str(number)
      else: return str(number)
    def _merge(t1, t2):
      """Helper function merge tables!"""
      tf = []
      t2t = [x[0] for x in t2]
      assert len(t1) == len(t2), "Lengths of values not the same, for some reason."
      tf.append(t1[0] + t2[0])
      for idx in range(len(t1)-1):
        tf.append(t1[idx+1]+t2[t2t.index(t1[idx+1][0])][1:])
      return tf
    column_widths = []
    ascii_table = []
    for val_idx, val in enumerate(vals):
      colors = []
      star_color_list = []
      exclude_cols = set() #clipped columns with NaN results.
      for star in self.stars:
        star_colors = self.stars[star].get(val, {}).keys()
        star_color_list.append(len(star_colors))
        for star_color in star_colors:
          if star_color not in colors:
            colors.append(star_color)
      for star_color in colors:
        if np.all([np.isnan(self.stars[x].get(val, {}).get(star_color, (np.NaN, np.NaN))[0]) for x in self.stars]):
          exclude_cols.add(star_color)
      color_corrected = list(set(colors).difference(exclude_cols))
      def _sort_alpha(x):
        j = 0.0
        d = "abcdefghijklmnopqrstuvwxyz"
        for idx, l in enumerate(x):
          j += (d.index(l.lower())-25.0)*math.pow(26.0, -idx)
        return j
      color_corrected = sorted(sorted(color_corrected, key=lambda elem: self.lambda_eff.get(elem.split("-")[1], 0.0) if len(elem.split("-")) > 1 else 0.0), key=lambda elem: self.lambda_eff.get(elem.split("-")[0], 0.0) if len(elem.split("-")) > 1 else _sort_alpha(elem))
      star_color_list = []
      for star in self.stars: #redone, removing old colors
        star_colors = [x for x in self.stars[star].get(val, {}).keys() if x not in exclude_cols]
        star_color_list.append(len(star_colors))
      if val_idx == 0:
        column_widths.append(np.zeros((len(self.stars)+1, np.max(star_color_list)+1)))
        ascii_table.append([["Star"] + color_corrected])
      #INITIAL ERROR FORMAT:
      #1.24123\pm0.24132
      else:
        column_widths.append(np.zeros((len(self.stars)+1, np.max(star_color_list))))
        ascii_table.append([color_corrected])
      for column_idx in range(column_widths[-1].shape[1]):
        column_widths[-1][0][column_idx] = len(ascii_table[-1][0][column_idx])
      #all_pos = np.all([color_data[1][0] > 0.0 for color_idx, color_data in enumerate([x for x in self.stars[star][val].items() if x[0] not in exclude_cols])]) #do this only on colors!
      all_pos = False
      #print "%s, %s" % (val, all_pos)
      for star_idx, star in enumerate(self.stars): #should be well-behaved, and have the same order as previous self.stars iteration
        if val_idx == 0: column_widths[-1][star_idx+1][0] = len(star) #intent-ative fix
        star_row = [star] + ["None"] * len(color_corrected)
        for color_idx, color_data in enumerate([x for x in self.stars[star][val].items() if x[0] not in exclude_cols]):
          if val == "spt": data = config.num_to_SpT(color_data[1][0])
          else: data = _sign(np.round(color_data[1][0], config.defaults["roundto"]), all_pos)
          value_string = "%s%s%s" % (data, self.criteria["%s|%s" % (val, color_data[0])], np.abs(np.round(color_data[1][1], config.defaults["roundto"])))
          if val_idx == 0: column_widths[-1][star_idx+1][color_corrected.index(color_data[0])+1] = len(value_string)
          else: column_widths[-1][star_idx+1][color_corrected.index(color_data[0])] = len(value_string)
          star_row[color_corrected.index(color_data[0])+1] = value_string
        ascii_table[-1].append(star_row)
      #Now, pad according to column_widths.
    column_widths = [np.max(column_width, 0).tolist() for column_width in column_widths]
    column_widths = reduce(lambda acc, ele: np.concatenate((acc, ele)), column_widths)
    ascii_table = reduce(_merge, ascii_table)
    for column_idx, column_width in enumerate(column_widths):
      ascii_table[0][column_idx] += " "*int(column_width - len(ascii_table[0][column_idx])) #compatibility with unicode
      for row_idx in range(len(ascii_table)-1):
        ascii_table[row_idx+1][column_idx] += " "*int(column_width - len(ascii_table[row_idx+1][column_idx])) #pad with spaces.
    #for header_idx in range(len(ascii_table[0])-1): #bugfix?
      #ascii_table[0][header_idx+1] = ascii_table[0][header_idx+1][:-header_idx-1]
    #ascii_table[0][0] += " "*2 #compatibility with unicode
    ascii_table = [[config.header(", ".join(vals))]] + ascii_table
    return "\n".join([" ".join(x) for x in ascii_table])
  def __str__(self):
    return self.get_table()
  def __repr__(self):
    return self.get_table()
  def del_star(self, names):
    """Delete a single star's data."""
    if type(names) == str: names = [names]
    for star_name in names:
      self.stars.pop(star_name, None)
  def del_color(self, color):
    """Delete a color in the internally loaded data, across all values."""
    for star in self.stars:
      for value in self.stars[star]:
        self.stars[star][value].pop(color, None)
  def del_value(self, value, color):
    """Delete a color from a specific value."""
    for star in self.stars:
      self.stars[star].get(value, {}).pop(color, None)
  def set_colors(self, colors):
    if type(colors) == str: colors = [colors]
    self.color_list = colors

class yso_val(object):
  """Provides a standard API for calculations.
  Usage: a yso_val by default only stores a single value, whether it
  * be Av or logL.
  A call to "keep" with the appropriate names of the variables that
  * are requested will cache them when they are created.
  This is useful when querying multiple properties from the same
  * dataset.
  To instantiate a yso_val bucket, run load_file and load_table.
  * Then, run load_data with appropriate colors on every unique data
  * instance.

  If a data bucket is exchanged, the yso_val object will reload the
  * index and prune all its indices automatically."""
  def __init__(self):
    self.deps = {
      "ocolor": (self.get_observed_colors),
      "icolor": (self.get_intrinsic_colors)#,
      #"av": (self.get_av_colors, ("ocolor","icolor")),
      #"avsed": (self.get_av_sed),
    }
    self.alias = {}
    self.I = interpolator() #Idiomatically, access yso_val.I to perform operations on the interpolator object after instantiating an instance of yso_val.
  def _load_index(self):
    """Reloaded each time the bucket is modified."""
    self.idx = list(enumerate(self.bucket.get_data("spt")[0]))
  def alias_color(self, color, tocolor):
    self.I.alias_color(color, tocolor)
    self.alias[color] = tocolor
  def resolve(self, target_names, names, data, errs):
    """Make sure the star ordering scheme is correct."""
    assert len(target_names) == len(names) == len(data) == len(errs), "Mismatched lengths of data: %s, %s, %s, %s" % (len(target_names), len(names), len(data), len(errs))
    target_data = [[], []]
    for name in target_names:
      target_data[0].append(data[names.index(name)])
      target_data[1].append(errs[names.index(name)])
    target_data[0] = np.array(target_data[0])
    target_data[1] = np.array(target_data[1])
    return target_data
  def load_file(self, filename = config.defaults["file"], extinction = config.defaults["extinction"], filters = config.defaults["filters"]):
    """Loads all the files traditionally needed for interpolation.
    * Many configurable options in config.py."""
    self.I.load_file(filename)
    extinction_law = np.genfromtxt(extinction)
    self.dA = self.I.interpolate([x[0] for x in extinction_law], [y[1]/config.filters["Av_scale"] for y in extinction_law]) #Converts a wavelength to a dA value.
    reddening_filters = np.genfromtxt(filters, skip_header=config.filters["skip_lines"], usecols=config.filters["cols"], dtype=None)
    self.lambda_eff = dict([[x[0],x[1]/config.filters["x_scale"]] for x in reddening_filters if x[0] != "?"]) #Converts a color to an effective wavelength (microns).
  def load_table(self, table = config.defaults["table"]):
    """Set as the current table object the appropriate table from the
    * interpolator object."""
    def inn(val):
      def innn(val2):
        return val in val2
      return innn
    self.table = self.I.tables[filter(inn(table), self.I.tables.keys())[0]] #Finds the particular table meeting specs.
  def load_data(self, bucket, exclude_colors=[]):
    self.bucket = bucket
    self.bucket.lambda_eff = self.lambda_eff ###hack
    colors = [x[0].split("-") for x in self.table["fields"] if "-" in x[0] and x[0].split("-")[0] not in exclude_colors and x[0].split("-")[1] not in exclude_colors] #Get all the colors except excluded colors.
    colors_corrected = colors
    for aliaskey in self.alias:
      colors_corrected = [[self.alias[aliaskey] if x[0] == aliaskey else x[0], self.alias[aliaskey] if x[1] == aliaskey else x[1]] for x in colors_corrected]
    self.set_color_list(colors_corrected)
    self._load_index()
  #def ensure_data(self, val = "phot", color = "value"):
    #data = self.bucket.get_data(val, color)
    #if np.all(np.isnan(data[1])): #all elements are NaN, so this table doesn't exist.
      #recursively call functions to calculate dependencies
  #FIELD-BASED
  def find_shortest_path(self, graph, start, end, path=[]):
    """From https://www.python.org/doc/essays/graphs/ on 6/23/14"""
    path = path + [start]
    if start == end:
      return path
    if not graph.has_key(start):
      return None
    shortest = None
    for node in graph[start]:
      if node not in path:
        newpath = self.find_shortest_path(graph, node, end, path)
        if newpath:
          if not shortest or len(newpath) < len(shortest):
            shortest = newpath
    return shortest
  def color_to_graph(self, color_list_arg):
    """Generates a graph from the colors, positive and forward.
    Accepts a list of color tuples. Assumes tuple is a directed edge
    * from the first color to the second color."""
    graph, pos_graph = {},{} # Directed edges hack.
    for f, t in color_list_arg:
      if not graph.has_key(f):
        graph[f] = [t]
        pos_graph[f] = [t]
      else:
        graph[f].append(t)
        pos_graph[f].append(t)
    for t, f in color_list_arg: #graph is bidirectional
      if not graph.has_key(f):
        graph[f] = [t]
      else:
        graph[f].append(t)
    return (graph, pos_graph)
  def set_color_list(self, color_list):
    graph, pos_graph = self.color_to_graph(color_list)
    self.color_list = []
    color_things = set([x[0] for x in color_list]+[x[1] for x in color_list])
    self.color_combinations = [list(x) for x in list(combinations(color_things, 2))] #adapted directly from previous iteration. may not be true if not all colors are accessible.
    for color_combination in self.color_combinations: #Orders the color_combination by increasing wavelength.
      if self.lambda_eff[color_combination[0]] > self.lambda_eff[color_combination[1]]:
        color_combination[0], color_combination[1] = color_combination[1], color_combination[0]
    for color_combination in self.color_combinations:
      path = self.find_shortest_path(graph, color_combination[0], color_combination[1])
      gen_path = []
      for idx in range(len(path) - 1):
        if pos_graph.has_key(path[idx]) and path[idx+1] in pos_graph[path[idx]]:  #forward-facing
          gen_path.append((path[idx], path[idx+1], 1))
        elif pos_graph.has_key(path[idx+1]) and path[idx] in pos_graph[path[idx+1]]:
          gen_path.append((path[idx+1], path[idx], -1))
      self.color_list.append(gen_path)
      self.colors = zip(self.color_list, self.color_combinations)
      self.graph = graph
      self.pos_graph = pos_graph
  #CALCULATIONS
  def get_extinction_law(self, color1, color2):
    """Generates the appropriate delta A/A_v with Mathis 1990.
    NOTE: this is still a color-by-color calculation! If a resolving method can be developed, this will be the first to take advantage of it."""
    extinction_law_colors = self.dA(self.lambda_eff[color1])-self.dA(self.lambda_eff[color2])
    return extinction_law_colors
  def get_observed_colors(self):
    for color_path, color_combo in self.colors:
      #(name_list, data_list, err_list)
      color_1 = self.bucket.get_data("phot", color_combo[0])
      color_2 = self.bucket.get_data("phot", color_combo[1])
      color_2[1], color_2[2] = self.resolve(color_1[0], color_2[0], color_2[1], color_2[2])
      color_delta = np.array(color_1[1]) - np.array(color_2[1])
      color_err = np.sqrt(np.square(color_1[2]) + np.square(color_2[2]))
      self.bucket.set_data(color_1[0], color_delta, color_err, val_name = "ocolor", color = "%s-%s" % (color_combo[0], color_combo[1]))
  def get_intrinsic_color(self, color_path, spt_list):
    """Generate an intrinsic color given a color_path. Example: [("B", "V", 1), ("U", "V", -1)]
    Generates intrinsic colors from the interpolation table."""
    color_sum = np.zeros(len(spt_list[0]))
    for color in color_path:
      color_sum += self.table["interpolation"]["SpT->%s-%s" % (color[0], color[1])](spt_list[1])*color[2]
    return (spt_list[0], color_sum)
  def get_intrinsic_colors(self):
    names_spt, spt, spt_errs = self.bucket.get_data("spt", "value")
    for color_path, color_combo in self.colors:
      names, color_sum = self.get_intrinsic_color(color_path, [names_spt, spt, spt_errs])
      names_p, color_sum_p = self.get_intrinsic_color(color_path, [names_spt, spt+spt_errs, spt_errs])
      names_n, color_sum_n = self.get_intrinsic_color(color_path, [names_spt, spt-spt_errs, spt_errs])
      color_sum_errs = np.fabs(color_sum_p - color_sum_n)/2.35
      self.bucket.set_data(names, color_sum, color_sum_errs, val_name = "icolor", color = "%s-%s" % (color_combo[0], color_combo[1]))
  def get_av(self):
    self.get_observed_colors()
    self.get_intrinsic_colors()
    ##self.get_extinction_law()
    #temporary, until dependencies are supported
    for color_path, color_combo in self.colors:
      ocolors = self.bucket.get_data("ocolor", "%s-%s" % (color_combo[0], color_combo[1]))
      icolors = self.bucket.get_data("icolor", "%s-%s" % (color_combo[0], color_combo[1]))
      icolors[1], icolors[2] = self.resolve(ocolors[0], icolors[0], icolors[1], icolors[2])
      extinction = [None, self.get_extinction_law(color_combo[0], color_combo[1]), None] #self.bucket.get_data("extinction", "%s-%s" % (colors1, colors2))
      #TODO: resolve o,i,e.
      av = (ocolors[1]-icolors[1])*np.reciprocal(extinction[1])
      #scale to particular band by scaling extinction_law_pure if necessary (like an A_lambda function)
      av = av.clip(min=0)#remove all negative values
      av_err = np.sqrt(np.square(ocolors[2]) + np.square(icolors[2]))*np.reciprocal(extinction[1]) #errors! TODO: look at
      self.bucket.set_data(ocolors[0], av, av_err, val_name = "av", color = "%s-%s" % (color_combo[0], color_combo[1]))
  def get_al(self):
    self.get_av() #TODO: with dependency management, al can use av_sed..
    for idx, color in enumerate([x[1] for x in self.colors]):
      av = self.bucket.get_data("av", "%s-%s" % (color[0], color[1]))
      al = av[1]*self.dA(self.lambda_eff[color[0]])
      al_err = av[2]
      self.bucket.set_data(av[0], al, al_err, val_name = "al", color = "%s-%s" % (color[0], color[1]))
  def red_chi_squared(self, observed, model):
    """Calculates the reduced chi-squared statistic for a single star.
    The equation used for calculating the Chi2 statistic is 1/(N-1)*sum((observed - model photometries)/sqrt(photometric error))

    :param observed: an ordered list of photometric magnitudes, model: an ordered list of photometric magnitudes corresponding to the values in observed."""
    #return (1.0/(len(observed)-1.0))*np.sum(np.square((observed-model))*np.reciprocal(np.var(observed)))
    return (1.0/(len(observed)-1.0))*np.sum(np.square((np.array(observed)-np.array(model))*np.reciprocal(np.sqrt(0.02))))
    #return (1.0/(len(observed)-1.0))*np.sum(np.square((observed-model)))
  def get_av_sed(self):
    """Uses SED fitting to calculate a probable Av for each star, using V-band photometry."""
    def get_err_from_chisq(point, av_tupl):
      """First, scale to a probability distribution."""
      av_tuple = sorted(av_tupl, key=lambda av_guess: av_guess[0])
      min_search_x2 = point[1]+1.0
      ### FIX errors in logl's (0.588)
      idx = [x[0] for x in av_tuple].index(point[0])
      left_idx = av_tuple[0][0]
      right_idx = av_tuple[-1][0]
      #go left
      for n in range(idx):
        if av_tuple[n][1] > min_search_x2:
          left_idx = av_tuple[idx-n-1][0]
      for n in range(len(av_tuple) - idx):
        if av_tuple[len(av_tuple) - n - 1][1] > min_search_x2:
          right_idx = av_tuple[len(av_tuple) - n - 1][0]
      return np.fabs(right_idx - left_idx)/2.0
    phot_v_stars, phot_v, phot_v_errs = self.bucket.get_data("phot", "V")
    virtual_photometry = {"V": (np.array(phot_v), phot_v_errs)}
    spt_list = self.bucket.get_data("spt", "value")
    spt_list[1], spt_list[2] = self.resolve(phot_v_stars, spt_list[0], spt_list[1], spt_list[2])
    av = np.empty_like(phot_v)
    avx = np.empty_like(phot_v)
    for color_path, band in [[x[0],x[1][1]] for x in self.colors if x[1][0]=="V"]:
      virtual_photometry[band] = (np.array(phot_v) - np.array(self.get_intrinsic_color(color_path, spt_list)[1]), phot_v_errs) #fix phot_v_errs (placeholder)
    best_fit_avs = {}
    errs = {}
    for star_idx, star in enumerate(zip(*self.bucket.get_value("phot"))):
      star, phot_dict = star[0], star[1]
      dream_dict = []
      for av_guess in config.av_sed:
        reddened_vp = {}
        for iter_color in set(phot_dict).intersection(virtual_photometry.keys()):
          reddened_vp[iter_color] = [av_guess, virtual_photometry[iter_color][0][star_idx] + (-1.0 + self.dA(self.lambda_eff.get(iter_color, np.NaN)))*av_guess] #deredden photometries
        obs_model = [(phot_dict[iterated_color][0], reddened_vp[iterated_color][1]) for iterated_color in reddened_vp]
        #err is obtained by assuming gaussian
        dream_dict.append((av_guess, self.red_chi_squared(*zip(*obs_model))))
      best_fit_avs[star] = sorted(zip([x[0] for x in dream_dict], [y[1] for y in dream_dict]), key=lambda chi_sq: chi_sq[1])[0]
      errs[star] = get_err_from_chisq(best_fit_avs[star], dream_dict)
    #add a probability distribution from the discrete points, sum up to one-sigma to get errors!
    self.bucket.set_data(phot_v_stars, [best_fit_avs[star][0] for star in phot_v_stars], [errs[star] for star in phot_v_stars], val_name = "av", color = "sed")
  def get_av_weighted(self):
    """If a value is missing, the program uses the available statistical weights.
    Weights are additive, based on the first and second color.
    A default weight is also used.
    Uncertainty is given as RMS."""
    self.get_av()
    star_list = self.bucket.get_data("spt", "value")[0]
    avs = [[x, [], []] for x in star_list]
    avw = [[], []]
    for color_path, color_combo in self.colors:
      stars, av, av_errs = self.bucket.get_data("av", color = "%s-%s" % (color_combo[0], color_combo[1]))
      curr_weight = config.stats.get(color_combo[0], config.stats["default"]) + config.stats.get(color_combo[1], config.stats["default"])
      for star, av_val, av_err in zip(stars, av, av_errs):
        if not np.isnan(av_val):
          av_tupl = avs[[x[0] for x in avs].index(star)]
          avs[[x[0] for x in avs].index(star)] = [star, av_tupl[1]+[av_val], av_tupl[2]+[curr_weight]]
    for av_tupl in avs:
      av_vals = np.array(av_tupl[1])
      weights = np.array(av_tupl[2])
      if len(av_vals) > 0:
        avw[0].append(np.sum(av_vals*weights)/np.sum(weights))
        avw[1].append(np.sqrt(1.0/float(len(av_vals))*1.0/(np.sum(np.square(weights)))*np.sum(np.square(av_vals*weights)))) #weighted rms
      else:
        avw[0].append(np.NaN)
        avw[1].append(np.NaN)
    self.bucket.set_data([x[0] for x in avs], avw[0], avw[1], val_name = "av", color = "weighted", criteria = ", rms: ")
  def bcv(self, spt):
    return self.table["interpolation"]["SpT->BCV"](np.array(spt))
  def get_bcv(self):
    stars, spt, spt_errs = self.bucket.get_data("spt", "value")
    self.bucket.set_data(stars, self.bcv(spt), np.fabs(self.bcv(spt+spt_errs) - self.bcv(spt-spt_errs))/2.35, val_name = "bcv", color = "bcv")
  def get_bcl(self):
    stars, spt, spt_errs = self.bucket.get_data("spt", "value")
    bcv = self.bcv(spt)
    bcl = []
    bcl_err = []
    mod_color = [x[1] for x in self.colors]
    for c_idx, color_combo in enumerate(mod_color):
      path = self.find_shortest_path(self.graph, color_combo[0], "V")
      gen_path = []
      for indx in range(len(path)-1): #throws a TypeError if color is not in the database.
        if self.pos_graph.has_key(path[indx]) and path[indx+1] in self.pos_graph[path[indx]]: #forward-facing
          gen_path.append((path[indx], path[indx+1], 1))
        elif self.pos_graph.has_key(path[indx+1]) and path[indx] in self.pos_graph[path[indx+1]]:
          gen_path.append((path[indx+1], path[indx], -1))
      bcl = bcv - self.get_intrinsic_color(gen_path, [stars, spt])[1]
      bcl_err = np.fabs(self.get_intrinsic_color(gen_path, [stars, spt+spt_errs])[1] - self.get_intrinsic_color(gen_path, [stars, spt-spt_errs])[1])/2.35
      self.bucket.set_data(stars, bcl, bcl_err, val_name = "bcl", color = color_combo[0])
  def get_logl(self):
    stars_d, dist, dist_errs = self.bucket.get_data("dist", "value")
    self.get_al()
    self.get_bcl()
    for color in [x[1] for x in self.colors]:
      stars_al, al, al_errs = self.bucket.get_data("al", "%s-%s" % (color[0], color[1]))
      stars_bcl, bcl, bcl_errs = self.bucket.get_data("bcl", color[0])
      stars_phot, phot, phot_errs = self.bucket.get_data("phot", color[0])
      al, al_errs = self.resolve(stars_d, stars_al, al, al_errs)
      bcl, bcl_errs = self.resolve(stars_d, stars_bcl, bcl, bcl_errs)
      phot, phot_errs = self.resolve(stars_d, stars_phot, phot, phot_errs)
      #print "%s, %s; %s, %s; %s, %s" % (str(np.all(np.isnan(al))), str(np.all(np.isnan(al_errs))), str(np.all(np.isnan(bcl))), str(np.all(np.isnan(bcl_errs))), str(np.all(np.isnan(phot))) ,str(np.all(np.isnan(phot_errs))))
      #match by ordering stars_al, stars_bcl.
      logl = (4.75+al+5*np.log10(dist/10.0)-phot-bcl)/2.5
      #print al, bcl, phot
      if not np.all(np.isnan(logl)):
        logl_err = 1/2.5*np.sqrt(np.square(al_errs)+np.square(dist_errs*np.reciprocal(dist*np.log(10)))+np.square(phot_errs)+np.square(bcl_errs))
        self.bucket.set_data(stars_d, logl, logl_err, val_name = "logl", color = "%s-%s" % (color[0], color[1]))
  def get_logl_sed(self):
    """Currently calculates logl for V-band photometry."""
    stars_d, dist, dist_errs = self.bucket.get_data("dist", "value")
    self.get_av_sed()
    self.get_bcv()
    stars_av, av, av_errs = self.bucket.get_data("av", "sed")
    stars_bcv, bcv, bcv_errs = self.bucket.get_data("bcv", "bcv")
    stars_phot, phot, phot_errs = self.bucket.get_data("phot", "V")
    av, av_errs = self.resolve(stars_d, stars_av, av, av_errs)
    bcv, bcv_errs = self.resolve(stars_d, stars_bcv, bcv, bcv_errs)
    phot, phot_errs = self.resolve(stars_d, stars_phot, phot, phot_errs)
    logl = (4.75+av+5*np.log10(dist/10.0)-phot-bcv)/2.5
    logl_err = 1/2.5*np.sqrt(np.square(av_errs)+np.square(dist_errs*np.reciprocal(dist*np.log(10)))+np.square(phot_errs)+np.square(bcv_errs))
    self.bucket.set_data(stars_d, logl, logl_err, val_name = "logl", color = "sed")
  def get_logl_weighted(self):
    """Currently calculates logl for V-band photometry."""
    stars_d, dist, dist_errs = self.bucket.get_data("dist", "value")
    self.get_av_weighted()
    self.get_bcv()
    stars_av, av, av_errs = self.bucket.get_data("av", "weighted")
    stars_bcv, bcv, bcv_errs = self.bucket.get_data("bcv", "bcv")
    stars_phot, phot, phot_errs = self.bucket.get_data("phot", "V")
    av, av_errs = self.resolve(stars_d, stars_av, av, av_errs)
    bcv, bcv_errs = self.resolve(stars_d, stars_bcv, bcv, bcv_errs)
    phot, phot_errs = self.resolve(stars_d, stars_phot, phot, phot_errs)
    logl = (4.75+av+5*np.log10(dist/10.0)-phot-bcv)/2.5
    logl_err = 1/2.5*np.sqrt(np.square(av_errs)+np.square(dist_errs*np.reciprocal(dist*np.log(10)))+np.square(phot_errs)+np.square(bcv_errs))
    self.bucket.set_data(stars_d, logl, logl_err, val_name = "logl", color = "weighted")
  def get_teff(self):
    """Generates the teff values as "teff"."""
    stars, spt, spt_errs = self.bucket.get_data("spt")
    teff = self.table["interpolation"]["SpT->Teff"](spt)
    teff_p = self.table["interpolation"]["SpT->Teff"](spt+spt_errs)
    teff_n = self.table["interpolation"]["SpT->Teff"](spt-spt_errs)
    teff_errs = (teff_p - teff_n)/2.35
    self.bucket.set_data(stars, teff, teff_errs, val_name = "teff", color = "value")
  def get_logt(self):
    """Generates the logt values as "logt"."""
    stars, spt, spt_errs = self.bucket.get_data("spt")
    teff = self.table["interpolation"]["SpT->Teff"](spt)
    teff_p = self.table["interpolation"]["SpT->Teff"](spt+spt_errs)
    teff_n = self.table["interpolation"]["SpT->Teff"](spt-spt_errs)
    logt = np.log10(teff)
    logt_errs = (np.log10(teff_p) - np.log10(teff_n))/2.35
    self.bucket.set_data(stars, logt, logt_errs, val_name = "logt", color = "value")
  def get_rsun(self):
    """Generates the rsun values."""
    self.get_logl()
    self.get_teff()
    lsun = 3.8935*math.pow(10.0,26)
    sigma = 5.670400*math.pow(10.0,-8)
    rsun_m = 1/(6.95508*math.pow(10.0,8))
    num_prefactor = math.sqrt(lsun/(4*np.pi*sigma))*rsun_m
    stars_teff, teff, teff_errs = self.bucket.get_data("teff")
    for color in [x[1] for x in self.colors]:
      stars_logl, logl, logl_errs = self.bucket.get_data("logl", "%s-%s" % (color[0], color[1]))
      logl, logl_errs = self.resolve(stars_teff, stars_logl, logl, logl_errs)
      rsun = num_prefactor*np.power(np.power(10.0,logl)*np.reciprocal(np.power(teff,4.0)),0.5)
      rsun_errs = num_prefactor*np.power(10.0, 0.5*logl)*np.sqrt(0.25*np.log(10)*np.log(10)*np.power(teff, -4.0)*np.square(logl_errs)+4.0*np.power(teff, -6.0)*np.square(teff_errs))
      self.bucket.set_data(stars_teff, rsun, rsun_errs, val_name = "rsun", color = "%s-%s" % (color[0], color[1]))
  def create_grid(self):
    """Not implemented yet, grids are for particular isochrones where you can't get a very good estimate."""
    return "grid"
  def load_isochrones(self, isochrone_grid = config.iso["grid"], isochrone_fn = config.loaded_isochrone):
    iso_data = isochrone_fn()
    #grid_x, grid_y = np.mgrid[min([x[2] for x in iso_data]):max([x[2] for x in iso_data]):150j,min([x[3] for x in iso_data]):max([x[3] for x in iso_data]):150j]
    #if isochrone_grid:
      #self.create_grid()
    #return [grid_x, grid_y]
    iso_data = [x for x in iso_data if not np.any(np.isnan(x))]
    self.isochrones = iso_data #age,mass,logt,logl
  def load_custom_isochrone(self, custom):
    """Allows an arbitrary custom isochrone to be loaded. The structure of an isochrone is a list of four-tuples, with the format (age, mass, logt, logl)."""
    self.isochrones = custom
  def generate_priors(self, age = config.prior["age"], mass = config.prior["mass"]):
    """Generate priors from the isochronal points!"""
    self.age_prior = age(self.isochrones)
    self.mass_prior = mass(self.isochrones)
  def bayesian(self, logl = "logl", logl_color = "value", logt = "logt", logt_color = "value"):
    """Specify the data label of your logl's or logt's saved in the bucket. These values must be precomputed, ie with a yso_val.get_logl_sed(), and then passed through as an argument.
    This function saves mass and age values with corresponding colors in the bucket.
    The isochrones and priors must have already been generated / loaded."""
    stars_l, logl_val, logl_errs = self.bucket.get_data(logl, logl_color)
    stars_t, logt_val, logt_errs = self.bucket.get_data(logt, logt_color)
    logl, logl_errs = self.resolve(stars_t, stars_l, logl_val, logl_errs)
    A, M, logT_model, logL_model = np.array(self.isochrones).T.tolist()
    star_list = []
    for idx, (name, logL_i, logLe_i, logT_i, logTe_i) in enumerate(zip(stars_l, logl_val, logl_errs, logt_val, logt_errs)):
        chi_squared = np.square((logT_model - logT_i)/logTe_i) + np.square((logL_model - logL_i)/logLe_i)
        prob = np.exp(-0.5*chi_squared)*self.age_prior*self.mass_prior
        probability_dist = sorted(zip(np.log10(A), M, prob), key=lambda tpl: tpl[2])
        best_fit_age, best_fit_mass = zip(*[x[0:2] for x in probability_dist])
        #if name == "HLTau":
            #res_x, res_y = 20, 20
            #hi_model_x, lo_model_x = np.max(np.log10(A)), np.min(np.log10(A)) #ages?
            #hi_model_y, lo_model_y = np.max(M), np.min(M) #masses?
            #img = np.zeros((res_x, res_y))
            #assuming each thing is a row!
            #bins_x, bins_y = np.arange(res_x+1)/float(res_x)*(hi_model_x - lo_model_x)+lo_model_x, np.arange(res_y+1)/float(res_y)*(hi_model_y - lo_model_y)+lo_model_y
            #for val_x, val_y, a_prob in probability_dist:
                #num_bin_x = int((val_x - lo_model_x)/(hi_model_x - lo_model_x)*float(res_x)) #find the bin we're in
                #if num_bin_x == int(res_x): num_bin_x -= 1
                #num_bin_y = int((val_y - lo_model_y)/(hi_model_y - lo_model_y)*float(res_y)) #find the other bin we're in
                #if num_bin_y == int(res_y): num_bin_y -= 1
                #img[num_bin_x][num_bin_y] += a_prob #assuming each row is a row..
            #plt.imshow(img, extent=[lo_model_y, hi_model_y, hi_model_x, lo_model_x], cmap="Greys", aspect="auto")
            #plt.title("%s - Age: %s $dex$, Mass: %s $M_{sun}$" % (name, best_fit_age[-1], best_fit_mass[-1]))
            #plt.ylabel("Age ($log_{10}(yr)$)")
            #plt.xlabel("Mass ($M_{sun}$)")
            #plt.show()
            #plt.clf()
        star_list.append([name, best_fit_age[-1], best_fit_mass[-1]])
    star_list = sorted(star_list, key = lambda some_star: some_star[1])
    #max_mass = np.max([aa[2] for aa in star_list])
    #min_mass = np.min([aa[2] for aa in star_list])
    #print [(z[2]-min_mass)/(max_mass-min_mass) for z in star_list]
    fig, ax = plt.subplots()
    barlist = ax.bar(np.arange(len(star_list)), [y[1] for y in star_list], width=1, color="grey")
    barlist[[ab[0] for ab in star_list].index("HLTau")].set_color("black")
    ax.set_xticks(np.arange(len(star_list)))
    ax.set_xticklabels([ab[0] for ab in star_list], rotation = 90, ha = "left")
    ax.set_xlim([0, len(star_list)])
    ax.set_title("Taurus-Aurigae YSO ages, Dartmouth 2014 tracks")
    ax.set_ylabel("Age ($log(M_{sun})$)")
    plt.subplots_adjust(bottom=0.25)
    plt.show()
    return True


#TODO: change parameters if segfault occurs
#TODO: weighted rms, weighted SED fitting, weighted logl, and SED value -> logl
