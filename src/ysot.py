import numpy as np
import matplotlib.pyplot as plt
taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
#limiter to prevent it taking 3 mins per run


#TODO: email list of av/logl/etc(outputs), colors (inputs)

def ocolors():
  calc.get_observed_colors()
  print bucket.get_table("ocolor")

def icolors():
  calc.get_intrinsic_colors()
  print bucket.get_table("icolor")

def old_av():
  calc.get_av()
  print bucket.get_table("av")

def av():
  calc.get_av()
  calc.get_av_weighted()
  calc.get_av_sed()
  print bucket.get_table("spt", "av")

def av_sed():
  calc.get_av_sed()
  print bucket.get_table("av_sed")

def al():
  calc.get_al()
  print bucket.get_table("al")


def bcl():
  calc.get_bcv()
  calc.get_bcl()
  print bucket.get_table("bcv", "bcl")

def logl():
  calc.get_logl()
  calc.get_logl_weighted()
  calc.get_logl_sed()
  print bucket.get_table("spt", "logl")

def teff():
  calc.get_teff()
  print bucket.get_table("teff")

def logt():
  calc.get_logt()
  print bucket.get_table("logt")

def rsun():
  calc.get_rsun()
  print bucket.get_table("spt", "rsun")

def iso_gen_web():
  import os
  for n in range(len(config.iso["resource"].split("/"))-2): #kind of hacky
    directory = "/".join(config.iso["resource"].split("/")[:-(len(config.iso["resource"].split("/"))-2)+n])
    if not os.path.exists(directory):
      os.makedirs(directory)
  def color_gen(best, bins, probs, colors_raw, levels):
    import math
    colors = colors_raw[:]
    levels = [0.01*x for x in levels]
    indx = 0 #for the extras.
    gen_colors = ["blue"] * len(bins)
    start_bar_idx = sorted([[math.fabs(best - x), bins_idx] for bins_idx, x in enumerate(bins)], key=lambda a: a[0])[0][1] #start from the closest "bar"
    extras = [[bins[start_bar_idx], bins[start_bar_idx]] for x in levels] #explicit ranges corresponding to probability levels
    radius_search = 1
    p_total = 0.0
    gen_colors[start_bar_idx] = colors[0]
    while True:
      if start_bar_idx >= radius_search:
        p_total += probs[start_bar_idx - radius_search]
        if p_total < levels[0]:
          gen_colors[start_bar_idx - radius_search] = colors[0]
          extras[indx][0] = bins[start_bar_idx - radius_search]
        elif len(colors) > 1:
          gen_colors[start_bar_idx - radius_search] = colors[1]
          extras[indx+1][1] = bins[start_bar_idx - radius_search]
          colors.pop(0)
          levels.pop(0)
          indx += 1
        elif len(colors) == 1:
          return (gen_colors, extras)
      if start_bar_idx + radius_search < len(bins):
        p_total += probs[start_bar_idx + radius_search]
        if p_total < levels[0]:
          gen_colors[start_bar_idx + radius_search] = colors[0]
          extras[indx][1] = bins[start_bar_idx + radius_search]
        elif len(colors) > 1:
          gen_colors[start_bar_idx + radius_search] = colors[1]
          extras[indx+1][1] = bins[start_bar_idx + radius_search]
          colors.pop(0)
          levels.pop(0)
          indx += 1
        elif len(colors) == 1:
          return (gen_colors, extras)
      if start_bar_idx - radius_search < 0 and start_bar_idx + radius_search >= len(bins): return (gen_colors, extras)
      radius_search += 1
  calc.get_logl()
  calc.get_teff()
  nAv, Av, Ave = bucket.get_data("av", "V-Ic")
  nL, logL, logLe = bucket.get_data("logl", "V-Ic")
  logL, logLe = calc.resolve(nAv, nL, logL, logLe)
  L = np.power(10.0, logL)
  Le = (np.power(10.0, logL + logLe) - np.power(10.0, logL - logLe))/2.35
  nT, T, Te = bucket.get_data("teff", "value")
  T, Te = calc.resolve(nAv, nT, T, Te)
  nspt, spt, spte = bucket.get_data("spt", "value")
  spt, spte = calc.resolve(nAv, nspt, spt, spte)
  import StringIO
  from mpl_toolkits.mplot3d import Axes3D
  from scipy import interpolate as interp
  import math
  from matplotlib.ticker import NullFormatter
  import StringIO, urllib, base64
  with open("BCAH98_iso.3_extend", "r") as openfile:
    txt = openfile.read().split("log t (yr) =  ")
    header, body = txt[0], txt[1:]
    ages = [math.pow(10.0, float(x.split("\n",1)[0])) for x in body]
    parsed = [[[ages[idx]]+y for y in np.genfromtxt(StringIO.StringIO(x.replace("--------------------------------------------------------------------------------", "#")), usecols=(0,1,3), skiprows=1).tolist()] for idx, x in enumerate(body)] #A, M, T, L
    points, zm, zt = zip(*[[(np.log10(item[2]), item[3]), item[1], item[0]] for sublist in parsed for item in sublist if not math.isnan(item[0]) and not math.isnan(item[1]) and not math.isnan(item[2]) and not math.isnan(item[3])])
    grid_x, grid_y = np.mgrid[min([x[0] for x in points]):max([x[0] for x in points]):150j,min([x[1] for x in points]):max([x[1] for x in points]):150j]
    Tf, Lf = np.power(10.0, grid_x.flatten()), np.power(10.0, grid_y.flatten())
    jm = interp.griddata(np.array(points), np.array(zm), (grid_x, grid_y), method="linear") #mass
    jmf = jm.flatten()
    jt = interp.griddata(np.array(points), np.array(zt), (grid_x, grid_y), method="linear") #age
    jtf = jt.flatten()
    #age_prior =  1.0 #as shown in the age distribution, an age prior is probably not needed
    #apply a simple bounded age prior.. is 0 if age is above 100 Myr.
    age_prior = np.empty_like(jtf)
    for idx, jtf_i in enumerate(jtf):
      if jtf_i > config.iso["MAXAGE"]:
        age_prior[idx] = 0.0
      else:
        age_prior[idx] = 1.0
    mass_prior = np.empty_like(jmf)
    for idx, jmf_i in enumerate(jmf):
      if jmf_i < 1:
        mass_prior[idx] = 0.093*math.pow(np.e, -((math.log10(jmf_i)-math.log10(0.2))**2)/(2*(0.55**2))) #mass prior, chabrier IMF (2003 review paper)
      else:
        mass_prior[idx] = 0.041*math.pow(jmf_i,-1.35) #\pm 0.3 on the exp.
    field_grid = np.zeros(shape=(75,75))
    bestMs = []
    bestlogAs = [] #change from T to A
    bestlogTs = []
    bestlogLs = []
    with open("template.html", "r") as file_t:
      template = file_t.read()
      images = []
      for idx, (L_i, Le_i, T_i, Te_i) in list(enumerate(zip(L, Le, T, Te))):
        bestlogTs.append(math.log10(T_i))
        bestlogLs.append(math.log10(L_i))
        chi_squared = np.square((Tf - T_i)/Te_i) + np.square((Lf - L_i)/Le_i)
        prob = np.exp(-0.5*chi_squared)*age_prior*mass_prior
        prob_flat = prob
        prob = prob.reshape(grid_x.shape)
        prob_yay= [x for x in zip(prob_flat, jtf, jmf) if not math.isnan(x[0]) and not math.isnan(x[1]) and not math.isnan(x[2])]
        bestP, bestT, bestM = sorted(prob_yay, key=lambda a: -a[0])[0]
        bestMs.append(bestM)
        bestlogAs.append(np.log10(bestT))
        grid_t, grid_m = np.mgrid[math.log10(min(jtf)):math.log10(config.iso["MAXAGE"]):75j, min(jmf):max(jmf):75j]
        jtfN, jmfN, prob_flatN = zip(*sorted([x for x in zip(jtf, jmf, prob_flat) if not math.isnan(x[0]) and not math.isnan(x[1]) and not math.isnan(x[2])], key=lambda a: a[0]))
        grid_z = interp.griddata((np.log10(np.array(jtfN)), np.array(jmfN)), np.array(prob_flatN), (grid_t, grid_m), method="cubic")
        grid_z = grid_z/np.sum(grid_z[~np.isnan(grid_z)])
        grid_plevels = sorted((grid_z[~np.isnan(grid_z)]).tolist(), reverse=True)
        def get_contour(grid_plevels, plevel):
          curr_plevel = 0.0
          plevel *= 0.01
          for contour_prob in grid_plevels:
            curr_plevel += contour_prob
            if curr_plevel > plevel: return contour_prob
        contour_levels_raw = sorted(config.iso["PLEVELS"])
        contour_levels = [get_contour(grid_plevels, plevel) for plevel in contour_levels_raw]
        #start plotting
        left, width = 0.07, 0.65
        bottom, height = 0.10, 0.60
        bottom_h = left_h = left+width+0.02
        ax_main_dim = [left, bottom, width, height]
        ax_barx_dim = [left, bottom_h, width, 0.15]
        ax_bary_dim = [left_h, bottom, 0.2, height]
        ax_hrd_dim = [left_h, bottom_h, 0.2, 0.15]
        #these are from http://matplotlib.org/examples/pylab_examples/scatter_hist.html
        plt.figure(1)
        ax_main = plt.axes(ax_main_dim)
        ax_main.imshow(grid_z, aspect="auto", extent=(grid_m[0][0],grid_m[-1][-1],grid_t[0][0],grid_t[-1][-1]), origin="higher")
        plt.ylabel("log(Age) $dex$")
        plt.xlabel("Mass $(M/M_{sun})$")
        #plt.colorbar()
        ax_main.scatter([bestM],[np.log10(bestT)], c=[0.5])
        colors = [str(x/float(len(contour_levels))) for x in range(len(contour_levels))]
        CS = plt.contour(grid_m, grid_t, grid_z, colors=colors, levels=contour_levels)
        #ax_main.clabel(CS, inline=1, fontsize=10, fmt="%1.7f")
        proxy = [plt.Rectangle((0,0),1,1, fc = color) for color in colors]
        plt.legend(proxy, [str(x)+"%" for x in contour_levels_raw]).get_frame().set_alpha(0.5)
        #plot the things on the sides~
        bins_t = np.array([x[0] for x in grid_t]) #yaxis
        bins_m = np.array(grid_m[0]) #xaxis
        ax_barx = plt.axes(ax_barx_dim)
        ax_bary = plt.axes(ax_bary_dim)
        ax_barx.xaxis.set_major_formatter(NullFormatter())
        ax_bary.yaxis.set_major_formatter(NullFormatter())
        width_m = [bins_m[indx+1]-bins_m[indx] for indx in range(len(bins_m)-1)]
        width_t = [bins_t[indx+1]-bins_t[indx] for indx in range(len(bins_t)-1)]
        #shade the bar graphs according to the probability distributions.
        mass_colors, mass_confidence_intervals = color_gen(bestM, bins_m.tolist(), np.nansum(grid_z, 0).tolist(), colors, contour_levels_raw)
        ax_barx.bar(bins_m.tolist(), np.nansum(grid_z, 0).tolist(), width = width_m+[np.mean(width_m)], linewidth = 0, align = "center", color = mass_colors)
        age_colors, age_confidence_intervals = color_gen(math.log10(bestT), bins_t.tolist(), np.nansum(grid_z, 1).tolist(), colors, contour_levels_raw)
        ax_bary.barh(bins_t.tolist(), np.nansum(grid_z, 1).tolist(), height = width_t+[np.mean(width_t)], linewidth = 0, align = "center", color = age_colors)
        ax_barx.set_xlim(ax_main.get_xlim())
        ax_barx.set_ylim((0.00, max(np.nansum(grid_z, 0))))
        ax_bary.set_ylim(ax_main.get_ylim())
        ax_bary.set_xlim((0.00, max(np.nansum(grid_z, 1))))
        ax_barx.set_title("%s [%s$\pm$%s star] ($log(A)=%s\pm%s dex, M=%s\pm%s M/M_{sun}$)" % (nAv[idx], config.num_to_SpT(spt[idx]), spte[idx], np.round(math.log10(bestT),3), np.round((age_confidence_intervals[contour_levels_raw.index(68.0)][1]-age_confidence_intervals[contour_levels_raw.index(68.0)][0])/2.35,3),np.round(bestM,3),np.round((mass_confidence_intervals[contour_levels_raw.index(68.0)][1]-mass_confidence_intervals[contour_levels_raw.index(68.0)][0])/2.35,3))) #0.588 issue
        ax_hrd = plt.axes(ax_hrd_dim)
        grid_z[np.isnan(grid_z)] = 0
        field_grid += grid_z ## attempting to create a "full probability distribution" for the field.
        for isochr in parsed[::config.iso["increment"]]:
          isochr = sorted(isochr, key=lambda a: a[1])
          ax_hrd.plot([math.log10(x[2]) for x in isochr], [x[3] for x in isochr], c = str([x[0] for x in isochr][0]/np.max(zt)))
        ax_hrd.scatter([math.log10(T_i)],[math.log10(L_i)])
        plt.gca().invert_xaxis()
        ax_hrd.set_xlabel("Temperature $(K)$")
        ax_hrd.set_ylabel("Luminosity $log(L/L_{sun})$")
        ax_hrd.yaxis.tick_right()
        ax_hrd.yaxis.set_ticks_position('both')
        ax_hrd.yaxis.set_label_position("right")
        ax_hrd.xaxis.tick_top()
        ax_hrd.xaxis.set_ticks_position('both')
        ax_hrd.xaxis.set_label_position("top")
        ax_hrd.text(3.8, -3.0, "$A_v=%s\pm%s$" % (np.around(Av[idx], config.defaults["roundto"]), np.around(Ave[idx], config.defaults["roundto"])))
        fig = plt.gcf()
        #imgdata = StringIO.StringIO()
        #fig.set_size_inches(12, 6)
        #fig.savefig(imgdata, format='png', dpi=100)
        #imgdata.seek(0)  # rewind the data
        #uri = 'data:image/png;base64,' + urllib.quote(base64.b64encode(imgdata.buf))
        #print idx
        #images.append('<img src = "%s"/>' % uri)
        fig.set_size_inches(12, 6)
        filename = str(idx)+".png"
        fig.savefig(config.iso["resource"]+filename, format="png", dpi=160)
        uri = "./resources/%s" % filename
        images.append('<img src = "%s"/>' % uri)
        plt.clf()
      for a in range(1): #execute once, but keep it in the same level for readability
        plt.figure(1)
        ax_main = plt.axes(ax_main_dim)
        ax_main.imshow(field_grid, aspect="auto", extent=(grid_m[0][0],grid_m[-1][-1],grid_t[0][0],grid_t[-1][-1]), origin="higher")
        plt.ylabel("log(Age) $dex$")
        plt.xlabel("Mass $(M/M_{sun})$")
        ax_main.scatter(bestMs,bestlogAs)
        #colors = [str(x/float(len(contour_levels))) for x in range(len(contour_levels))]
        #CS = plt.contour(grid_m, grid_t, grid_z, colors=colors, levels=contour_levels)
        proxy = [plt.Rectangle((0,0),1,1, fc = color) for color in colors]
        plt.legend(proxy, [str(x)+"%" for x in contour_levels_raw]).get_frame().set_alpha(0.5)
        #plot the things on the sides~
        bins_t = np.array([x[0] for x in grid_t]) #yaxis
        bins_m = np.array(grid_m[0]) #xaxis
        ax_barx = plt.axes(ax_barx_dim)
        ax_bary = plt.axes(ax_bary_dim)
        ax_barx.xaxis.set_major_formatter(NullFormatter())
        ax_bary.yaxis.set_major_formatter(NullFormatter())
        width_m = [bins_m[indx+1]-bins_m[indx] for indx in range(len(bins_m)-1)]
        width_t = [bins_t[indx+1]-bins_t[indx] for indx in range(len(bins_t)-1)]
        ##shade the bar graphs according to the probability distributions.
        #mass_colors, mass_confidence_intervals = color_gen(bestM, bins_m.tolist(), np.nansum(grid_z, 0).tolist(), colors, contour_levels_raw)
        ax_barx.bar(bins_m.tolist(), np.nansum(grid_z, 0).tolist(), width = width_m+[np.mean(width_m)], linewidth = 0, align = "center")#, color = mass_colors)
        #age_colors, age_confidence_intervals = color_gen(math.log10(bestT), bins_t.tolist(), np.nansum(grid_z, 1).tolist(), colors, contour_levels_raw)
        ax_bary.barh(bins_t.tolist(), np.nansum(grid_z, 1).tolist(), height = width_t+[np.mean(width_t)], linewidth = 0, align = "center")#, color = age_colors)
        ax_barx.set_xlim(ax_main.get_xlim())
        ax_barx.set_ylim((0.00, max(np.nansum(grid_z, 0))))
        ax_bary.set_ylim(ax_main.get_ylim())
        ax_bary.set_xlim((0.00, max(np.nansum(grid_z, 1))))
        #ax_barx.set_title("%s [%s star] ($log(A)=%s\pm%s dex, M=%s\pm%s M/M_{sun}$)" % ([x[7] for x in taurus][idx], SpT_taurus[idx],np.round(math.log10(bestT),3),np.round((age_confidence_intervals[contour_levels_raw.index(68.0)][1]-age_confidence_intervals[contour_levels_raw.index(68.0)][0])/2.35,3),np.round(bestM,3),np.round((mass_confidence_intervals[contour_levels_raw.index(68.0)][1]-mass_confidence_intervals[contour_levels_raw.index(68.0)][0])/2.35,3))) #0.588 issue
        ax_hrd = plt.axes(ax_hrd_dim)
        for isochr in parsed[::config.iso["increment"]]:
          isochr = sorted(isochr, key=lambda a: a[1])
          ax_hrd.plot([math.log10(x[2]) for x in isochr], [x[3] for x in isochr], c = str([x[0] for x in isochr][0]/np.max(zt)))
        ax_hrd.scatter(bestlogTs, bestlogLs)
        plt.gca().invert_xaxis()
        ax_hrd.set_xlabel("Temperature $(K)$")
        ax_hrd.set_ylabel("Luminosity $log(L/L_{sun})$")
        ax_hrd.yaxis.tick_right()
        ax_hrd.yaxis.set_ticks_position('both')
        ax_hrd.yaxis.set_label_position("right")
        ax_hrd.xaxis.tick_top()
        ax_hrd.xaxis.set_ticks_position('both')
        ax_hrd.xaxis.set_label_position("top")
        fig = plt.gcf()
        #imgdata = StringIO.StringIO()
        #fig.set_size_inches(12, 6)
        #fig.savefig(imgdata, format='png', dpi=100)
        #imgdata.seek(0)  # rewind the data
        #uri = 'data:image/png;base64,' + urllib.quote(base64.b64encode(imgdata.buf))
        #print idx
        #images.append('<img src = "%s"/>' % uri)
        fig.set_size_inches(12, 6)
        filename = "field.png"
        fig.savefig(config.iso["resource"]+filename, format="png", dpi=160)
        uri = "./resources/%s" % filename
        images.append('<img src = "%s"/>' % uri)
        plt.clf()
      #replaced_template = template.replace("{}", "\n".join(images))
      replaced_template = template.replace("{}", "\n".join(images))
      open_f = open(config.iso["filename"], "w")
      open_f.write(replaced_template)
      open_f.close()

def iso():
  calc.get_logl_sed()
  calc.get_logt()
  calc.load_isochrones()
  calc.generate_priors()
  return calc.bayesian("logl", "sed")

def cluster():
  calc.get_logl_sed()
  calc.get_logt()
  calc.load_isochrones()
  calc.generate_priors()
  return calc.bayesian_cluster("Taurus", "logl", "sed")


def av_diag():
  calc.get_av()
  err_medians = [np.nanmedian([y[1] for y in x["av"].values()]) for x in bucket.stars.values()]
  err_stds = [np.nanstd([y[0] for y in x["av"].values()]) for x in bucket.stars.values()]
  plt.plot(err_medians, err_stds, 'bo')
  plt.xlabel("Error medians on Taurus")
  plt.ylabel("Standard deviations on Taurus")
  plt.savefig("taurus.av_err.pdf")

#For checking the EEM_dwarf_UBVIJHK_colors_Teff colors:
def check_interp(xname = "SpT",yname = "Teff"):
  plt.xlabel(xname)
  plt.ylabel(yname)
  if xname == "SpT": example_xs = np.linspace(20.,120.)
  elif xname == "Teff": example_xs = np.linspace(2000., 9000.)
  plt.title("%s vs %s with 'Fallback data'" % (xname, yname))
  plt.plot(example_xs, calc.table["original_interp"]["%s->%s" % (xname, yname)](example_xs), label="Old interpolation")
  plt.plot(example_xs, calc.table["interpolation"]["%s->%s" % (xname, yname)](example_xs), label="With Dwarf table")
  plt.legend()
  plt.show()

#l = iso()
#plt.imshow(l)
#plt.show()

#av_diag()
#cluster()

import time
start = time.time()
for i in range(1):
  #end limiter
  SpT_taurus = [str(x[0]) for x in taurus] #""""Rc": [x[2] for x in taurus],"""
  photometry = {"V": [x[1] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  names = [x[7] for x in taurus]
  distances = [140.0 for x in taurus]
  import yso as yso
  import config #temp, iso
  #Bucket init
  bucket = yso.yso_data()
  for phot in photometry.items():
    bucket.set_data(names, phot[1], val_name = "phot", color = phot[0])
    #Load photometries
  bucket.set_data(names, SpT_taurus, val_name = "spt")
  bucket.set_data(names, distances, val_name = "dist")
  #bucket.set_data(names, np.repeat(1.4,len(names)), val_name = "rveil")
  #bucket.set_data(names, np.repeat(1.,len(names)), val_name = "iveil")
  #Load spectral types
  #Calculator init
  calc = yso.yso_val()
  calc.alias_color("K", "Ks")
  calc.load_file()
  calc.load_table()

  calc.load_data(bucket) #Attached
  calc.get_av()
  print bucket.get_value("av")
print time.time() - start
