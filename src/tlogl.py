import numpy as np
import matplotlib.pyplot as plt
import yso

def gen():
  import yso
  import numpy as np
  import matplotlib.pyplot as plt
  taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
  SpT_taurus = [x[0] for x in taurus] #""""Rc": [x[2] for x in taurus],"""
  photometry = {"V": [x[1] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  l = yso.yso(SpT_taurus, photometry, [140.0 for x in taurus])
  l.load_file("pecaut_mamajek.vot")
  l.load_table("table6")
  with open("test.dat", "w") as towrite:
    towrite.write(l.ascii_list(l.A_v(), [x[7] for x in taurus], "Av") + "\n\n")
    towrite.write(l.ascii_list(l.logl(), [x[7] for x in taurus], "log(L/Lsun)") + "\n\n")
    towrite.write(l.ascii_list(l.rsun(), [x[7] for x in taurus], "R/Rsun"))

def gen_new():
  taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
  SpT_taurus = [x[0] for x in taurus]
  photometry = {"V": [x[1] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  l = yso.yso("table6", "pecaut_mamajek.vot")
  with open("test.dat", "w") as towrite:
    l.load_data(SpT_taurus, photometry, [140.0 for x in taurus])
    towrite.write(l.ascii_list(l.A_v(), [x[7] for x in taurus], "Av"))

def chisq():
  import yso
  import numpy as np
  import matplotlib.pyplot as plt
  taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
  SpT_taurus = [x[0] for x in taurus] #""""Rc": [x[2] for x in taurus],"""
  photometry = {"V": [x[1] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  l = yso.yso_sed(SpT_taurus, photometry, [140.0 for x in taurus])
  l.load_file("pecaut_mamajek.vot")
  l.load_table("table6")
  xs = [x for x in np.genfromtxt("test-avs.dat", skip_header=1, usecols=(2))]
  a=plt.scatter(xs,l.A_v(True).tolist())
  #b=plt.scatter(xs,l.A_v(False).tolist(),c="green")
  plt.xlabel("$A_v$ from $V-J$")
  plt.ylabel("Computed best-fit $A_v$")
  #plt.legend((a,b),(r"Lowest $\chi ^2$",r"$\chi ^2 = 1$"))
  plt.show()

def compare6():
  taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
  SpT_taurus = [x[0] for x in taurus]
  photometry = {"V": [x[1] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  l = logL.logL(SpT_taurus, photometry, [140.0 for x in taurus])
  l.load_file("pecaut_mamajek.vot")
  l.load_table("table6")
  data = l.getlist([x[7] for x in taurus])
  color_table_idx = ["-".join(x) for x in l.A.color_combo]
  color_dict = {"Ic-J": "hrd.data.iij", "V-Ic": "hrd.data.ivi", "J-H": "hrd.data.jjh", "H-Ks": "hrd.data.khk", "V-Ks": "hrd.data.vvk"}
  f, axes = plt.subplots(len(color_dict))
  for idx, [color, filename] in enumerate(color_dict.items()):
    prev_calc_data = [float(x) for x in np.genfromtxt(filename, usecols=(2)).tolist()]
    interp_data = [x[color_table_idx.index(color)+1] for x in data]
    y = np.linspace(min([min(prev_calc_data),min(interp_data)]),max([max(prev_calc_data),max(interp_data)]))
    axes[idx].set_xlabel("Pre log(L)")
    axes[idx].set_ylabel("New log(L)")
    axes[idx].set_title("$%s$ log(L) plot (%s mean diff)" % (color, str(np.round(np.mean(interp_data)-np.mean(prev_calc_data), 3))))
    axes[idx].scatter(prev_calc_data,interp_data)
    axes[idx].plot(y,y)
  fig = plt.gcf()
  fig.set_size_inches(12,32)
  plt.savefig('logl-compare.png',dpi=100)

def compare1():
  taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
  SpT_taurus = [x[0] for x in taurus]
  photometry = {"V": [x[1] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  l = yso.yso(SpT_taurus, photometry, [140.0 for x in taurus])
  l.load_file("pecaut_mamajek.vot")
  l.load_table("table6")
  data = l.get_list(l.logl(),[x[7] for x in taurus])
  color_table_idx = ["-".join(x) for x in l.color_combo]
  color_dict = {"Ic-J": "hrd.data.iij", "V-Ic": "hrd.data.ivi", "J-H": "hrd.data.jjh", "H-Ks": "hrd.data.khk", "V-Ks": "hrd.data.vvk"}
  #f, axes = plt.subplots(len(color_dict))
  for idx, [color, filename] in enumerate(color_dict.items()):
    if idx==0:
      prev_calc_data = [float(x) for x in np.genfromtxt(filename, usecols=(2)).tolist()]
      interp_data = [x[color_table_idx.index(color)+1] for x in data]
      y = np.linspace(min([min(prev_calc_data),min(interp_data)]),max([max(prev_calc_data),max(interp_data)]))
      plt.xlabel("Pre log(L)")
      plt.ylabel("New log(L)")
      plt.title("$%s$ log(L) plot (%s mean diff)" % (color, str(np.round(np.mean(interp_data)-np.mean(prev_calc_data), 3))))
      plt.scatter(prev_calc_data,interp_data)
      plt.plot(y,y)
  plt.show()


def timetest():
  import yso
  import numpy as np
  import matplotlib.pyplot as plt
  import datetime
  a = datetime.datetime.now()
  taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
  SpT_taurus = [x[0] for x in taurus] #""""Rc": [x[2] for x in taurus],"""
  photometry = {"V": [x[1] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  l = yso.yso_sed(SpT_taurus, photometry, [140.0 for x in taurus])
  l.load_file("pecaut_mamajek.vot")
  l.load_table("table6")
  avs = []
  logls = []
  rsuns = []
  iters = 10
  for x in range(iters):
    avs.append(l.ascii_list(l.A_v(), [x[7] for x in taurus], "Av") + "\n\n")
    logls.append(l.ascii_list(l.logl(), [x[7] for x in taurus], "log(L/Lsun)") + "\n\n")
    rsuns.append(l.ascii_list(l.rsun(), [x[7] for x in taurus], "R/Rsun"))
  print "%s elapsed for %s stars across avs, logls, and rsun calculations" % (str(datetime.datetime.now()-a), str(76*iters))
  print "%s seconds elapsed per iteration"  % str(((datetime.datetime.now()-a).days*86400.0+(datetime.datetime.now()-a).seconds+(datetime.datetime.now()-a).microseconds/1000000.0)/iters)
  print "%s seconds predicted for a query of 10,000 stars across Av, logl, and rsuns" % str(((datetime.datetime.now()-a).days*86400.0+(datetime.datetime.now()-a).seconds+(datetime.datetime.now()-a).microseconds/1000000.0)/76.0/iters*10000)
