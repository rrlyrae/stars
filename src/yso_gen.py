import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
clusters = {"ucl": [146., 13.],
            "lcc": [120., 10.],
            "scocen": [143., 19.],
            "uppersco": [149., 19.]}
for cluster in clusters.keys():
  cluster_data = pd.read_csv("ysoc."+cluster+".csv")
  names_cluster_data = cluster_data["Star Name"].tolist()
  spt_cluster_data = pd.to_numeric(cluster_data["spt_num"], errors='coerce')
  photometry = {"V": pd.to_numeric(cluster_data["V"], errors='coerce'),
                "Rc": pd.to_numeric(cluster_data["Rc"], errors='coerce'),
                "Ic": pd.to_numeric(cluster_data["Ic"], errors='coerce'),
                "J": pd.to_numeric(cluster_data["tm_J"], errors='coerce'),
                "H": pd.to_numeric(cluster_data["tm_H"], errors='coerce'),
                "Ks": pd.to_numeric(cluster_data["tm_K"], errors='coerce'),
                "W3": pd.to_numeric(cluster_data["W_3p4"], errors='coerce'),
                "W4": pd.to_numeric(cluster_data["W_4p6"], errors='coerce')}

  phot_error = {"V": pd.to_numeric(cluster_data["V_e"], errors='coerce'),
                "Rc": pd.to_numeric(cluster_data["Rc_e"], errors='coerce'),
                "Ic": pd.to_numeric(cluster_data["Ic_e"], errors='coerce'),
                "J": pd.to_numeric(cluster_data["tm_J_e"], errors='coerce'),
                "H": pd.to_numeric(cluster_data["tm_H_e"], errors='coerce'),
                "Ks": pd.to_numeric(cluster_data["tm_K_e"], errors='coerce'),
                "W3": pd.to_numeric(cluster_data["W_3p4_e"], errors='coerce'),
                "W4": pd.to_numeric(cluster_data["W_4p6_e"], errors='coerce')}


  RAs = pd.to_numeric(cluster_data["RA"], errors='coerce')
  DECs = pd.to_numeric(cluster_data["DEC"], errors='coerce')

  dat = {"plx": cluster_data["plx"].tolist(),
         "plx_e": cluster_data["plx_e"].tolist(),
         "pm_ra": cluster_data["pm_ra"].tolist(),
         "pm_ra_e": cluster_data["pm_ra_e"].tolist(),
         "pm_dec": cluster_data["pm_dec"].tolist(),
         "pm_dec_e": cluster_data["pm_dec_e"].tolist(),
         "gaia_g": cluster_data["gaia_g"].tolist(),
         "gaia_g_e": cluster_data["gaia_g_e"].tolist(),
         "Cluster": cluster_data["Cluster"].tolist()}

  distances = []
  dist_errs = []
  for x, y in zip(pd.to_numeric(cluster_data["plx"], errors='coerce').tolist(), pd.to_numeric(cluster_data["plx_e"], errors='coerce').tolist()):
    if np.isnan(x):
      distances.append(clusters[cluster][0])
      dist_errs.append(clusters[cluster][1])
    else:
      distances.append(1000./x)
      dist_err = np.abs(1000./(x+y) - 1000./(x-y))/2.35
      dist_errs.append(dist_err)

  import yso as yso
  import config #temp, iso

  #Bucket init
  bucket = yso.yso_data()
  for band in photometry.keys():
    bucket.set_data(names_cluster_data, photometry[band], phot_error[band], val_name = "phot", color = band)
    #Load photometries
  bucket.set_data(names_cluster_data, spt_cluster_data, val_name = "spt", color = "spt")
  bucket.set_data(names_cluster_data, distances, dist_errs, val_name = "dist")
  bucket.set_data(names_cluster_data, RAs, val_name = "spt", color = "RA")
  bucket.set_data(names_cluster_data, DECs, val_name = "spt", color = "Dec")
  #bucket.set_data(names, np.repeat(1.4,len(names)), val_name = "rveil")
  #bucket.set_data(names, np.repeat(1.,len(names)), val_name = "iveil")
  #Load spectral types
  for key, val in dat.items():
    bucket.set_data(names_cluster_data, val, val_name = "logt", color = key) #place these values under the "logt" group so it will be output with the logt data.

  ##Calculator init
  calc = yso.yso_val()
  calc.alias_color("K", "Ks")
  calc.load_file()
  calc.load_table()

  calc.load_data(bucket) #Attached

  def av():
    calc.get_av_weighted()
    open_f = open(cluster+".av", "w")
    open_f.write(bucket.get_table_pd("spt", "av").encode("UTF-8"))
    open_f.close()

  def logl():
    calc.get_logl_weighted()
    open_f = open(cluster+".logl", "w")
    open_f.write(bucket.get_table_pd("spt", "logl").encode("UTF-8"))
    open_f.close()

  def logt():
    calc.get_logt()
    open_f = open(cluster+".logt", "w")
    open_f.write(bucket.get_table_pd("spt", "logt").encode("UTF-8"))
    open_f.close()

  def rsun():
    calc.get_rsun_weighted()
    open_f = open(cluster+".rsun", "w")
    open_f.write(bucket.get_table_pd("spt", "rsun").encode("UTF-8"))
    open_f.close()

  def colors():
    calc.get_observed_colors()
    open_f = open(cluster+".phot", "w")
    open_f.write(bucket.get_table_pd("spt", "ocolor").encode("UTF-8"))
    open_f.close()

  def dist():
    open_f = open(cluster+".dist", "w")
    open_f.write(bucket.get_table_pd("dist").encode("UTF-8"))
    open_f.close()

  #logt()
  #logl()
  #rsun()
  #av()
  #colors()
  #dist()
