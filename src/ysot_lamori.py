import numpy as np
import matplotlib.pyplot as plt

la = np.genfromtxt("LamOri_photometry.tbl", skip_header=1, usecols=(0,7,8,9,10,11,12,13,14,15,16,17,18,19), dtype=None)
#V    V_e   R_c   R_c_e I_c   I_c_e J      J_e    H      H_e    K      K_e    SpT
def idx_of(some_arr):
    def helper_arr(idx, no_numerics=False):
        if no_numerics:
            return [np.NaN if x[idx] == "None" else x[idx] for x in some_arr]
        else: return [np.NaN if x[idx] == "None" else float(x[idx]) for x in some_arr]
    return helper_arr

la_data = idx_of(la)
photometry = {"V": [la_data(1), la_data(2)], "Rc": [la_data(3), la_data(4)], "Ic": [la_data(5), la_data(6)], "J": [la_data(7), la_data(8)], "H": [la_data(9), la_data(10)], "K": [la_data(11), la_data(12)]}
spt = la_data(-1, True)

import yso
import config
distances = [340.0 for x in la] #check!
names = [x[0] for x in la]

bucket = yso.yso_data()
for phot in photometry.items():
  bucket.set_data(names, phot[1][0], val_name = "phot", color = phot[0])
  #Load photometries
bucket.set_data(names, spt, val_name = "spt")
bucket.set_data(names, distances, val_name = "dist")

calc = yso.yso_val()
calc.alias_color("K", "Ks")
calc.load_file()
calc.load_table()

calc.load_data(bucket)

#we might have a lot of spares after calc.get_av()
calc.get_av()
calc.get_av_sed()
#get rid of all stars that have all nan values.
"""a_data = [bucket.get_data("av", "Ic-J"), bucket.get_data("av", "Ic-H"), bucket.get_data("av", "J-H"), bucket.get_data("av", "J"), bucket.get_data("av", "H"), bucket.get_data("av", "K")]
stars_to_delete = set(names)
stars_to_save = set()
for color_data in a_data:
  for star_number in range(len(color_data[1])):  
    print color_data[1][star_number]
    if not np.isnan(color_data[1][star_number]):
      print star_number
      stars_to_save.add(color_data[0][star_number])"""
      
for star_data in bucket.stars.items():
  if np.all([np.isnan(x[0]) for x in star_data[1]["av"].values()]):
    bucket.del_star(star_data[0])

#bucket.del_star(list(stars_to_delete.difference_update(stars_to_save)))

#print bucket.get_table("av")
calc.get_logl()
calc.get_logt()

calc.load_isochrones()
calc.generate_priors()
calc.bayesian(logl_color="Ic-J")
