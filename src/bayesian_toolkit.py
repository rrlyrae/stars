import numpy as np
def find_conf_interval_68_95(counts):
  all_counts = sorted(counts.flatten(), key = lambda x: -x)
  all_counts_sum = sum(all_counts)
  total = 0.
  level_68 = -np.inf
  level_95 = -np.inf
  for idx in range(len(counts.flatten())):
      total += all_counts[idx]/all_counts_sum
      if total > 0.68:
          if np.isinf(level_68):
              level_68 = all_counts[idx]
      if total > 0.95:
          if np.isinf(level_95):
              level_95 = all_counts[idx]
  return [level_95, level_68]
def cdf_from_pdf(xs, pdf):
    cdf = np.zeros_like(pdf)
    for idx in range(len(pdf)):
        cdf[idx] = np.trapz(pdf[:idx+1], xs[:idx+1])
    cdf /= cdf[-1]
    return cdf

def confidence(xs, p_xs, interv):
    """Function calculates a numerical confidence interval.
    It searches from the left-hand side and interpolates to right.
    arguments: xs - parameter array
               p_xs - probability density array
               interv - proportion of the conf. interv."""
    from scipy import interpolate
    cdf = cdf_from_pdf(xs, p_xs) #Create CDF.
    cdf_interp = interpolate.interp1d(cdf, xs)
    #interpolate on the y-axis so we can put a p level and get an x.
    best_confidence_left = -np.inf
    best_confidence_right = np.inf
    for idx in range(len(p_xs)):
        bottom_p = cdf[idx] #bottom prob value
        confidence_left = xs[idx] #bottom x value
        if bottom_p + interv < 1.:
            confidence_right = cdf_interp(bottom_p + interv)
            #find the appropriate interpolated x value
            #that corresponds to the other side of the conf interv.
            if best_confidence_right - best_confidence_left > \
            confidence_right - confidence_left:
                best_confidence_left = confidence_left
                #replace "best" left
                best_confidence_right = float(confidence_right)
                #replace "best" right
    mean_conf = xs[np.argmax(p_xs)]
    if best_confidence_left > mean_conf:
        best_confidence_left = mean_conf
    if best_confidence_right < mean_conf:
        best_confidence_right = mean_conf
    return [best_confidence_left, mean_conf,
            best_confidence_right]
    #return left, peak location, and right confidence intervals.

def bayesian_single(logt, logt_err, logl, logl_err, dartmouth_logt, dartmouth_logl, dartmouth_age, dartmouth_mass,
                  age_prior, mass_prior, star_name):
  import numpy as np
  import StringIO
  import matplotlib.pyplot as plt
  import numpy as np
  import base64
  from matplotlib.ticker import NullFormatter
  from scipy.spatial import ConvexHull
  #create hulls
  points = np.array(zip(np.log10(dartmouth_age), dartmouth_mass))
  hull = ConvexHull(points)
  points_hrd = np.array(zip(dartmouth_logt, dartmouth_logl))
  hull_hrd = ConvexHull(points_hrd)
  #end of hulls
  logl_err_clip = np.clip(logl_err, 0.1, np.inf)
  if np.any(np.isnan([logt,
         logt_err,
         logl,
         logl_err_clip])):
      return None
  else:
      plt.clf()
      chisq = np.square((dartmouth_logt - logt)/logt_err) +\
      np.square((dartmouth_logl - logl)/logl_err_clip)
      best_fit = age_prior*mass_prior*np.exp(-.5*chisq)
      norm_best_fit = 1./hull.area/np.nansum(best_fit)*best_fit
      best_a = np.log10(dartmouth_age[np.nanargmax(norm_best_fit)])
      best_m = dartmouth_mass[np.nanargmax(norm_best_fit)]
      level = np.inf
      prob = 0.
      for idx1, x in enumerate(sorted(norm_best_fit, key = lambda x: -x)):
          prob += x*hull.area
          if prob > .99:
              level = x
              break
      logages = np.log10(dartmouth_age[norm_best_fit >= level])
      range_logage = 5.*(np.min(logages)-np.mean(logages))+np.mean(logages),5.*(np.max(logages)-np.mean(logages))+np.mean(logages)
      masses = dartmouth_mass[norm_best_fit >= level]
      range_mass = 5.*(np.min(masses)-np.mean(masses))+np.mean(masses),5.*(np.max(masses)-np.mean(masses))+np.mean(masses)
      if range_mass[0] == range_mass[1]:
          range_mass = [range_mass[0] - 0.1, range_mass[1] + 0.1]
      cond = np.bitwise_and(np.bitwise_and(range_logage[1] >= np.log10(dartmouth_age), np.log10(dartmouth_age) >= range_logage[0]),
      np.bitwise_and(range_mass[1] >= dartmouth_mass, dartmouth_mass >= range_mass[0]))
      range_logage = [np.min(np.log10(dartmouth_age[cond])), np.max(np.log10(dartmouth_age[cond]))]
      range_mass = [np.min(dartmouth_mass[cond]), np.max(dartmouth_mass[cond])]
      #plotting
      nullfmt = NullFormatter()
      # definitions for the axes
      left, width = 0.1, 0.65
      bottom, height = 0.1, 0.65
      bottom_h = left_h = left + width + 0.02

      rect_scatter = [left, bottom, width, height]
      rect_histx = [left, bottom_h, width, 0.2]
      rect_histy = [left_h, bottom, 0.2, height]
      rect_hrd = [left_h, bottom_h, 0.2, 0.2]
      rect_colorbar = [left_h + 0.22, bottom, 0.05, 0.87]
      # start with a rectangular Figure
      fig = plt.figure(1, figsize=(12, 12))
      axScatter = plt.axes(rect_scatter)
      axHistx = plt.axes(rect_histx)
      axHisty = plt.axes(rect_histy)
      axHistx.xaxis.set_major_formatter(nullfmt)
      axHisty.yaxis.set_major_formatter(nullfmt)
      ax_hrd = plt.axes(rect_hrd)
      ax_hrd.xaxis.set_major_formatter(nullfmt)
      ax_hrd.yaxis.set_major_formatter(nullfmt)
      tricontourf_obj = axScatter.tricontourf(np.log10(dartmouth_age[cond]),
                    dartmouth_mass[cond],
                    norm_best_fit[cond])
      levels = find_conf_interval_68_95(norm_best_fit[cond])
      axScatter.tricontour(np.log10(dartmouth_age[cond]),
                    dartmouth_mass[cond],
                    norm_best_fit[cond],levels,colors=["brown", "red"])
      tricontourf_obj.set_cmap(plt.cm.viridis)
      axScatter.legend(fancybox = True, numpoints = 1, fontsize=14, bbox_to_anchor = (1.47, -.05))
      axScatter.set_xlabel("$log_{10}$ Age ($yr$)")
      axScatter.set_ylabel("Mass ($M_\\odot$)")
      num_bins = 100
      histx_x = []
      histx_width = []
      histx = np.zeros(num_bins)
      histy_x = []
      histy_width = []
      histy = np.zeros(num_bins)
      for idx_hist in range(num_bins):
          range_logage_idx = [range_logage[0] + idx_hist/float(num_bins)*(range_logage[1] - range_logage[0]),\
                              range_logage[0] + (idx_hist+1.)/float(num_bins)*(range_logage[1] - range_logage[0])]
          histx_width.append(range_logage_idx[1] - range_logage_idx[0])
          cond_histx = np.bitwise_and(range_logage_idx[1] >= np.log10(dartmouth_age),\
                                      np.log10(dartmouth_age) >= range_logage_idx[0])
          histx_x.append(range_logage[0] + idx_hist/float(num_bins)*(range_logage[1] - range_logage[0]))
          if np.sum(cond_histx) > 1:
              norm_best_fit_histx = [y[0] for y in sorted(zip(norm_best_fit[cond_histx], dartmouth_mass[cond_histx]),key=lambda x:x[1])]
              sorted_mass_histx = sorted(dartmouth_mass[cond_histx])
              u, indices = np.unique(sorted_mass_histx, return_index=True)
              norm_best_fit_histx = np.array(norm_best_fit_histx)[indices]
              sorted_mass_histx = np.array(sorted_mass_histx)[indices]
              notnan = np.logical_not(np.isnan(norm_best_fit_histx))
              histx[idx_hist] = np.abs(np.trapz(np.array(norm_best_fit_histx)[notnan], np.array(sorted_mass_histx)[notnan]))
          elif np.sum(cond_histx) == 1:
              histx[idx_hist] = 0.
          range_mass_idx = [range_mass[0] + idx_hist/float(num_bins)*(range_mass[1] - range_mass[0]),\
                            range_mass[0] + (idx_hist+1.)/float(num_bins)*(range_mass[1] - range_mass[0])]
          histy_width.append(range_mass_idx[1] - range_mass_idx[0])
          cond_histy = np.bitwise_and(range_mass_idx[1] >= dartmouth_mass,\
                                      dartmouth_mass >= range_mass_idx[0])
          histy_x.append(range_mass[0] + idx_hist/float(num_bins)*(range_mass[1] - range_mass[0]))
          if np.sum(cond_histy) > 1:
              norm_best_fit_histy = [y[0] for y in sorted(zip(norm_best_fit[cond_histy], dartmouth_age[cond_histy]),key=lambda x:x[1])]
              sorted_age_histy = sorted(np.log10(dartmouth_age[cond_histy]))
              u, indices = np.unique(sorted_age_histy, return_index=True)
              norm_best_fit_histy = np.array(norm_best_fit_histy)[indices]
              sorted_age_histy = np.array(sorted_age_histy)[indices]
              notnan = np.logical_not(np.isnan(norm_best_fit_histy))
              histy[idx_hist] = np.abs(np.trapz(np.array(norm_best_fit_histy)[notnan], np.array(sorted_age_histy)[notnan]))
          elif np.sum(cond_histy) == 1:
              histy[idx_hist] = 0
      histx/=np.trapz(histx, histx_x)
      axHistx.bar(histx_x, histx, histx_width)
      histy/=np.trapz(histy, histy_x)
      axHisty.barh(histy_x, histy, histy_width)
      #conf interval
      x_c = np.array(histx_x + [histx_x[-1]])
      y_c = histx
      left_c, mean_c, right_c = confidence((x_c[:-1] + x_c[1:])/2., y_c, 0.68)
      #print "68% conf interval, age:", left_c, mean_c, right_c
      axHistx.axvline(left_c, color = "red")
      axHistx.axvline(mean_c, color = "orange")
      axHistx.axvline(right_c, color = "red")
      left_age_interv = left_c
      mean_age_interv = mean_c
      right_age_interv = right_c
      left_c, mean_c, right_c = confidence((x_c[:-1] + x_c[1:])/2., y_c, 0.95)
      #print "95% conf interval, age:", left_c, mean_c, right_c
      axHistx.axvline(left_c, color = "brown")
      axHistx.axvline(right_c, color = "brown")
      x_c = np.array(histy_x + [histy_x[-1]])
      y_c = histy
      left_c, mean_c, right_c = confidence((x_c[:-1] + x_c[1:])/2., y_c, 0.68)
      #print "68% conf interval, mass:", left_c, mean_c, right_c
      axHisty.axhline(left_c, color = "red")
      axHisty.axhline(mean_c, color = "orange")
      axHisty.axhline(right_c, color = "red")
      left_mass_interv = left_c
      mean_mass_interv = mean_c
      right_mass_interv = right_c
      left_c, mean_c, right_c = confidence((x_c[:-1] + x_c[1:])/2., y_c, 0.95)
      #print "95% conf interval, mass:", left_c, mean_c, right_c
      axHisty.axhline(left_c, color = "brown")
      axHisty.axhline(right_c, color = "brown")
      axScatter.plot([],[],color = "orange", label = "Mean")
      axScatter.plot([],[],color = "red", label = "$1 \\sigma$")
      axScatter.plot([],[],color = "brown", label = "$2 \\sigma$")
      ttl = plt.suptitle("Age/mass Bayesian: " + star_name +\
                        " $M = %.2f ^ {+%.2f} _ {-%.2f} M_\\odot$ $A = %.2f ^ {+%.2f} _ {-%.2f} Myr$" % (mean_mass_interv,\
                        right_mass_interv-mean_mass_interv,mean_mass_interv-left_mass_interv,\
                        np.power(10.,mean_age_interv - 6.),\
                        np.power(10.,right_age_interv-6.) - np.power(10.,mean_age_interv-6.),\
                        np.power(10.,mean_age_interv-6.) - np.power(10.,left_age_interv-6.)),y=1.01)
      #end conf interval
      axScatter.plot(best_a,
          best_m,
          marker="*",
          markersize=10,
          color="white",
          alpha = 1.0,
          label = "Peak")
      axScatter.set_xlim(range_logage)
      axScatter.set_ylim(range_mass)
      axHistx.set_xlim(axScatter.get_xlim())
      axHisty.set_ylim(axScatter.get_ylim())
      axScatter.scatter(np.log10(dartmouth_age[cond]), dartmouth_mass[cond], marker='+', c="black", alpha=0.25, label="Dartmouth 2014")
      cb = plt.colorbar(tricontourf_obj, cax = fig.add_axes(rect_colorbar))
      ax_hrd.invert_xaxis()
      for simplex in hull_hrd.simplices:
          ax_hrd.plot(points_hrd[simplex, 0], points_hrd[simplex, 1], 'k-')
      ax_hrd.errorbar(logt,
          logl,
                   xerr = logt_err,
                   yerr = logl_err,
                   ecolor='b',
                   fmt=",",
                   color='b')
      leg = axScatter.legend(fancybox = True, numpoints = 1, fontsize=14, bbox_to_anchor = (1.47, -.05))
      str_buf = StringIO.StringIO()
      plt.savefig(str_buf, format="png", bbox_extra_artists=(leg,ttl,), bbox_inches='tight')
      str_buf.seek(0)
      b64plot = '<img src="data:image/png;base64,%s" />' % base64.b64encode(str_buf.read())
      #plt.savefig("members/" + cluster_list[cluster_idx] + "_" + cl_name[idx] +".png", bbox_extra_artists=(leg,ttl,), bbox_inches='tight')
      plt.clf()
      return b64plot

import yso
def load_dartmouth():
  """Assumes Dartmouth tracks are in directory ./dartmouth_tracks/
  Currently, Dartmouth tracks correspond to 375,286 points in isochrone space.
  Loading all these points takes on the order of a dozen seconds on a modern PC. (2014)
  TODO: Resample this space as necessary!"""
  import os
  import StringIO
  import random
  directory = "./dartmouth_tracks/"
  points = [] #Age (yr), Mass (Msun), logT, logL
  files = [f for f in os.listdir(directory) if os.path.isfile(directory+f)]
  for filename in files:
    with open(directory+filename, "r") as f:
      isotxt = f.read()
      if len(isotxt) > 0:
        curr_mass = float(isotxt.split("M=")[1].split(" X=")[0])
        raw_points = np.genfromtxt(StringIO.StringIO(isotxt), usecols = (0,1,3)).tolist()
        for point in raw_points:
          if True: #random.random() < 1.0/375.286:
            points.append([point[0], curr_mass, point[1], point[2]])
  return points
#calc = yso.yso_val()
#calc.load_isochrones(isochrone_grid = False, isochrone_fn = load_dartmouth)
#calc.generate_priors()
#iso = np.array(calc.isochrones)
#dartmouth_age = iso.T[0]
#dartmouth_mass = iso.T[1]
#dartmouth_logt = iso.T[2]
#dartmouth_logl = iso.T[3]
#age_prior = calc.age_prior
#mass_prior = calc.mass_prior

#b64 = bayesian_single(3.7, 0.05, 0.5, 0.1, dartmouth_logt, dartmouth_logl, dartmouth_age, dartmouth_mass,
#                    age_prior, mass_prior, "hi")
