import logL
import numpy as np
import matplotlib.pyplot as plt

def gen():
  taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
  SpT_taurus = [x[0] for x in taurus]
  photometry = {"V": [x[1] for x in taurus], "Rc": [x[2] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  A = logL.A(SpT_taurus, photometry)
  A.load_file("pecaut_mamajek.vot")
  A.load_table("table5")
  return A.ascii_A_v([x[7] for x in taurus])

def compare5():
  taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
  SpT_taurus = [x[0] for x in taurus]
  photometry = {"V": [x[1] for x in taurus], "Rc": [x[2] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  A = logL.A(SpT_taurus, photometry)
  A.load_file("pecaut_mamajek.vot")
  A.load_table("table5")
  data = A.list_A_v([x[7] for x in taurus])
  color_table_idx = ["-".join(x) for x in A.color_combo]
  color_dict = {"Ic-J": "hrd.data.iij", "Rc-Ic": "hrd.data.iri", "V-Ic": "hrd.data.ivi", "J-H": "hrd.data.jjh", "H-Ks": "hrd.data.khk", "V-Ks": "hrd.data.vvk"}
  f, axes = plt.subplots(len(color_dict))
  for idx, [color, filename] in enumerate(color_dict.items()):
    prev_calc_data = [float(x) for x in np.genfromtxt(filename, usecols=(3)).tolist()]
    interp_data = [x[color_table_idx.index(color)+1] for x in data]
    y = np.linspace(0,max([max(prev_calc_data),max(interp_data)]))
    axes[idx].set_xlabel("Old $A_v$")
    axes[idx].set_ylabel("New $A_v$")
    axes[idx].set_title("$%s A_v$ difference (%s mean diff)" % (color, str(np.round(np.mean(interp_data)-np.mean(prev_calc_data), 3))))
    axes[idx].scatter(prev_calc_data,interp_data)
    axes[idx].plot(y,y)
  fig = plt.gcf()
  fig.set_size_inches(12,32)
  plt.savefig('color-compare-1.png',dpi=100)


def compare6():
  taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
  SpT_taurus = [x[0] for x in taurus]
  photometry = {"V": [x[1] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  A = logL.A(SpT_taurus, photometry)
  A.load_file("pecaut_mamajek.vot")
  A.load_table("table6")
  data = A.list_A_v([x[7] for x in taurus])
  color_table_idx = ["-".join(x) for x in A.color_combo]
  color_dict = {"Ic-J": "hrd.data.iij", "V-Ic": "hrd.data.ivi", "J-H": "hrd.data.jjh", "H-Ks": "hrd.data.khk", "V-Ks": "hrd.data.vvk"}
  f, axes = plt.subplots(len(color_dict))
  for idx, [color, filename] in enumerate(color_dict.items()):
    prev_calc_data = [float(x) for x in np.genfromtxt(filename, usecols=(3)).tolist()]
    interp_data = [x[color_table_idx.index(color)+1] for x in data]
    y = np.linspace(0,max([max(prev_calc_data),max(interp_data)]))
    axes[idx].set_xlabel("Pre $A_v$")
    axes[idx].set_ylabel("New $A_v$")
    axes[idx].set_title("$%s$ color difference (%s mean diff)" % (color, str(np.round(np.mean(interp_data)-np.mean(prev_calc_data), 3))))
    axes[idx].scatter(prev_calc_data,interp_data)
    axes[idx].plot(y,y)
  fig = plt.gcf()
  fig.set_size_inches(12,32)
  plt.savefig('color-compare-2.png',dpi=100)
