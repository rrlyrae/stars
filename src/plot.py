import matplotlib.pyplot as plt
import numpy as np
import yso

#Plots an ASCII or CSV file readable by numpy's genfromtxt method.

#class data(object):
#  def __init__(self):
#    pass
#  def add(self,x,y):
#    plt.plot(x,y)

def plot_extinction_laws():
  a = np.genfromtxt("extinction.mathis.dat")
  ax, ay = [x[0] for x in a], [y[1]/3.55 for y in a]
  b = np.genfromtxt("extinction.whitney.dat")
  bx, by = [x[0] for x in b], [y[1]/211.4 for y in b]
  plt.plot(ax, ay, label="Mathis 1990")
  plt.plot(bx, by, label="Whitney 2007")
  c = np.loadtxt("reddening.filters", skiprows=14, usecols=(2,3))
  cx, cy = [x[0]/10000.0 for x in c], [y[1] for y in c]
  plt.scatter(cx, cy)
  plt.xlabel("Wavelength $(\mu m)$")
  plt.ylabel("Reddening $(A_\lambda/A_v)$")
  plt.title("Extinction Law Plot")
  plt.loglog()
  plt.legend()
  plt.show()

def plot_ext_law_range():
  a = np.genfromtxt("extinction.mathis.dat")
  ax, ay = [x[0] for x in a], [y[1]/3.55 for y in a]
  b = np.genfromtxt("extinction.whitney.dat")
  bx, by = [x[0] for x in b], [y[1]/211.4 for y in b]
  plt.plot(ax, ay, label="Mathis 1990")
  plt.plot(bx, by, label="Whitney 2007")
  c = np.loadtxt("reddening.filters", skiprows=14, usecols=(2,3))
  cx, cy = [x[0]/10000.0 for x in c], [y[1] for y in c]
  plt.scatter(cx, cy, marker="o")
  d = np.loadtxt("old_extinction", usecols=(1,2))
  dx, dy = [x[0] for x in d], [y[1] for y in d]
  plt.scatter(dx, dy, marker="s")
  plt.xlabel("Wavelength $(\mu m)$")
  plt.ylabel("Reddening $(A_\lambda/A_v)$")
  plt.title("Extinction Law Plot")
  plt.legend()
  plt.axis([0.3,5.0,0,2.0])
  plt.show()

def plot_ext_law_range_new():
  a = np.genfromtxt("extinction.mathis.dat")
  ax, ay = [x[0] for x in a], [y[1]/3.55 for y in a]
  h = interpolate.interpolator()
  fn_a = h.interpolate(ax, ay)
  c = np.loadtxt("reddening.filters", skiprows=14, usecols=(2,3))
  cx, cy = [x[0]/10000.0 for x in c], [y[1]-fn_a(y[0]/10000.0) for y in c]
  plt.scatter(cx, cy, marker="o")
  d = np.loadtxt("old_extinction", usecols=(1,2))
  dx, dy = [x[0] for x in d], [y[1]-fn_a(y[0]) for y in d]
  plt.scatter(dx, dy, marker="s")
  plt.xlabel("Wavelength $(\mu m)$")
  plt.ylabel("Reddening Diff (from Mathis 1990) $(A_\lambda/A_v)$")
  plt.title("Extinction Law Plot")
  plt.legend()
  plt.axis([0.3,5.0,-0.1,0.1])
  plt.show()

def plot_filters():
  c = np.loadtxt("reddening.filters", skiprows=14, usecols=(2,3))
  cx, cy = [x[0]/10000.0 for x in c], [y[1] for y in c]
  plt.scatter(cx, cy)
  plt.xlabel("Wavelength $(\mu m)$")
  plt.ylabel("Reddening $(A_\lambda/A_v)$")
  plt.title("Reddening Scatterplot with $\lambda_{eff}$ of Filters")
  plt.show()

def plot_teff_logg():
  h = interpolate.interpolator()
  h.preload_axis("logg")
  h.load_file("pecaut_mamajek_loggcolors.vot")
  Teff,logg,UB,BV,VIc,RcIc,JH,HKs,VKs,KsW1,KsW2,KsW3,KsW4,VVt,BtVt,VHp=[x[1] for x in h.tables.items()[0][1]["data"].items()]
  plt.scatter(Teff, logg)
  plt.ylabel("$log(g)$ (dex)")
  plt.xlabel("Teff $(K)$")
  plt.title("$T_{eff}$ - Surface Gravity relation")
  plt.xscale("log")
  x = np.linspace(1000,100000, num=100000)
  rel = 3.79+0.64*5040/x
  plt.plot(x, rel)
  plt.show()

def plot_colors():
  h = yso.interpolator()
  h.preload_axis("logg")
  h.load_file("pecaut_mamajek_loggcolors.vot")
  Teff,logg = [x[1] for x in h.tables.items()[0][1]["data"].items()][0:2]
  plot_idx = zip("UB,BV,VIc,RcIc,JH,HKs,VKs,KsW1,KsW2,KsW3,KsW4,VVt,BtVt,VHp".split(","),[x[1] for x in h.tables.items()[0][1]["data"].items()][2:])
  num_plots = len(plot_idx)-1
  if num_plots % 2 == 1: num_plots+=1
  f, pre_axes = plt.subplots(num_plots/2, 2) #sharex='col', sharey='row'
  axes = []
  for some_axis in pre_axes:
    for axis in some_axis:
      #print axis
      axes.append(axis)
  for idx in range(len(plot_idx)-1):
    axes[idx].set_ylabel("$%s-%s$ (mags)" % (plot_idx[idx+1][0][:[plot_idx[idx+1][0].index(x) for x in plot_idx[idx+1][0][1:] if x.isupper()][0]], plot_idx[idx+1][0][[plot_idx[idx+1][0].index(x) for x in plot_idx[idx+1][0][1:] if x.isupper()][0]:]))
    axes[idx].set_xlabel("$%s-%s$ (mags)" % (plot_idx[idx][0][:[plot_idx[idx][0].index(x) for x in plot_idx[idx][0][1:] if x.isupper()][0]], plot_idx[idx][0][[plot_idx[idx][0].index(x) for x in plot_idx[idx][0][1:] if x.isupper()][0]:]))
    #maxteff, minteff = max(Teff), min(Teff)
    #plt.scatter(UB, BV, c=[(x-3.0)/2.0 for x in logg])
    colors = dict(zip([x/2.0 for x in range(6,11,1)], reversed("red,orange,yellow,green,blue".split(","))))
    UB3, BV3 = zip(*[[x[0], x[1]] for x in zip(plot_idx[idx][1], plot_idx[idx+1][1], logg) if x[2]==3.0])
    UB35, BV35 = zip(*[[x[0], x[1]] for x in zip(plot_idx[idx][1], plot_idx[idx+1][1], logg) if x[2]==3.5])
    UB4, BV4 = zip(*[[x[0], x[1]] for x in zip(plot_idx[idx][1], plot_idx[idx+1][1], logg) if x[2]==4.0])
    UB45, BV45 = zip(*[[x[0], x[1]] for x in zip(plot_idx[idx][1], plot_idx[idx+1][1], logg) if x[2]==4.5])
    UB5, BV5 = zip(*[[x[0], x[1]] for x in zip(plot_idx[idx][1], plot_idx[idx+1][1], logg) if x[2]==5.0])
    p3 = axes[idx].scatter(UB3, BV3, c=colors[3.0], edgecolor="none")
    p35 = axes[idx].scatter(UB35, BV35, c=colors[3.5], edgecolor="none")
    p4 = axes[idx].scatter(UB4, BV4, c=colors[4.0], edgecolor="none")
    p45 = axes[idx].scatter(UB45, BV45, c=colors[4.5], edgecolor="none")
    p5 = axes[idx].scatter(UB5, BV5, c=colors[5.0], edgecolor="none")
    if idx == 0: axes[idx].set_title("Color plots")
    #plt.gray()
  axes[-1].legend((p3,p35,p4,p45,p5), ("$log(g)=3.0$","$3.5$","$4.0$","$4.5$","$5.0$"))
  #f.subplots_adjust(hspace=0)
  #plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)
  fig = plt.gcf()
  fig.set_size_inches(12,24)
  plt.savefig('colorplot-1.png',dpi=100)

def hrd(rand=False):
  import yso
  import numpy as np
  import matplotlib.pyplot as plt
  taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
  SpT_taurus = [x[0] for x in taurus] #""""Rc": [x[2] for x in taurus],"""
  photometry = {"V": [x[1] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  l = yso.yso(SpT_taurus, photometry, [140.0 for x in taurus])
  l.load_file("pecaut_mamajek.vot")
  l.load_table("table6")
  colors = [str(x) for x in np.arange(1.0,0,-0.08)]
  logt = l.logt().tolist()
  if rand: #a jitter up to 0.05, but not exceeding.
    import random
    logt = [x+(random.random()-0.5)*0.1 for x in logt]
  logl = l.logl().tolist()
  scatterer = []
  for idx, logl in enumerate(logl):
    scatterer.append(plt.scatter(logt, logl, c=colors[idx]))
  plt.legend(scatterer, ["-".join(x) for x in l.color_combo][:len(logl)])
  plt.gca().invert_xaxis()
  plt.ylabel("$log(\\frac{L}{L_{sun}})$ (dex)")
  plt.xlabel("$log(T_{eff})$ (dex)")
  plt.title("HR Diagram of selected Taurus YSO's")
  plt.show()

def hrd0(rand=False):
  import yso
  import numpy as np
  import matplotlib.pyplot as plt
  taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
  SpT_taurus = [x[0] for x in taurus] #""""Rc": [x[2] for x in taurus],"""
  photometry = {"V": [x[1] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  l = yso.yso(SpT_taurus, photometry, [140.0 for x in taurus])
  l.load_file("pecaut_mamajek.vot")
  l.load_table("table6")
  colors = [str(x) for x in np.arange(1.0,0,-0.08)]
  logt = l.logt().tolist()
  if rand: #a jitter up to 0.05, but not exceeding.
    import random
    logt = [x+(random.random()-0.5)*0.1 for x in logt]
  logl = l.logl().tolist()
  plt.scatter(logt, logl[3])
  #scatterer = []
  #for idx, logl in enumerate(logl):
    #scatterer.append(plt.scatter(logt, logl, c=colors[idx]))
  #plt.legend(scatterer, ["-".join(x) for x in l.color_combo][:len(logl)])
  plt.gca().invert_xaxis()
  plt.ylabel("$log(\\frac{L}{L_{sun}})$ (dex)")
  plt.xlabel("$log(T_{eff})$ (dex)")
  plt.title("HR Diagram of selected Taurus YSO's")
  plt.show()

"""def Av_colors():
  taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
  SpT_taurus = [x[0] for x in taurus] 
  photometry = {"V": [x[1] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  l = yso.yso(SpT_taurus, photometry, [140.0 for x in taurus])
  l.load_file("pecaut_mamajek.vot")
  l.load_table("table6")
  dereddened_photometry = dict([(x[0], x[1]-l.A_v()[idx]*l.dA_gen(l.lambda_eff[x[0]])) for idx, x in enumerate(l.photometry.items())])
  ocolors = [dereddened_photometry[x[0]]-dereddened_photometry[x[1]] for x in l.color_combo]
  m = yso.yso_sed(SpT_taurus, photometry, [140.0 for x in taurus])
  m.load_file("pecaut_mamajek.vot")
  m.load_table("table6")
  sed_dereddened_photometry = dict([(x[0], x[1]-m.A_v()[idx]*m.dA_gen(m.lambda_eff[x[0]])) for idx, x in enumerate(m.photometry.items())])
  sed_ocolors = [sed_dereddened_photometry[x[0]]-sed_dereddened_photometry[x[1]] for x in m.color_combo]
  h = yso.interpolator()
  h.preload_axis("logg")
  h.load_file("pecaut_mamejek_loggcolors.vot")
  Teff,logg = [x[1] for x in h.tables.items()[0][1]["data"].items()][0:2]
  plot_idx = zip("UB,BV,VIc,RcIc,JH,HKs,VKs,KsW1,KsW2,KsW3,KsW4,VVt,BtVt,VHp".split(","),[x[1] for x in h.tables.items()[0][1]["data"].items()][2:])
  num_plots = len(plot_idx)-1
  if num_plots % 2 == 1: num_plots+=1
  f, pre_axes = plt.subplots(num_plots/2, 2) #sharex='col', sharey='row'
  axes = []
  for some_axis in pre_axes:
    for axis in some_axis:
      #print axis
      axes.append(axis)
  for idx in range(len(plot_idx)-1):
    axes[idx].set_ylabel("$%s-%s$ (mags)" % (plot_idx[idx+1][0][:[plot_idx[idx+1][0].index(x) for x in plot_idx[idx+1][0][1:] if x.isupper()][0]], plot_idx[idx+1][0][[plot_idx[idx+1][0].index(x) for x in plot_idx[idx+1][0][1:] if x.isupper()][0]:]))
    axes[idx].set_xlabel("$%s-%s$ (mags)" % (plot_idx[idx][0][:[plot_idx[idx][0].index(x) for x in plot_idx[idx][0][1:] if x.isupper()][0]], plot_idx[idx][0][[plot_idx[idx][0].index(x) for x in plot_idx[idx][0][1:] if x.isupper()][0]:]))
    if plot_idx[idx+1][0][:[plot_idx[idx+1][0].index(x) for x in plot_idx[idx+1][0][1:] if x.isupper()][0]]
    y = ("%s-%s" % (, plot_idx[idx+1][0][[plot_idx[idx+1][0].index(x) for x in plot_idx[idx+1][0][1:] if x.isupper()][0]:]))
    x = ("%s-%s" % (plot_idx[idx][0][:[plot_idx[idx][0].index(x) for x in plot_idx[idx][0][1:] if x.isupper()][0]], plot_idx[idx][0][[plot_idx[idx][0].index(x) for x in plot_idx[idx][0][1:] if x.isupper()][0]:]))
    #maxteff, minteff = max(Teff), min(Teff)
    #plt.scatter(UB, BV, c=[(x-3.0)/2.0 for x in logg])
    colors = dict(zip([x/2.0 for x in range(6,11,1)], reversed("red,orange,yellow,green,blue".split(","))))
    UB3, BV3 = zip(*[[x[0], x[1]] for x in zip(plot_idx[idx][1], plot_idx[idx+1][1], logg) if x[2]==3.0])
    UB35, BV35 = zip(*[[x[0], x[1]] for x in zip(plot_idx[idx][1], plot_idx[idx+1][1], logg) if x[2]==3.5])
    UB4, BV4 = zip(*[[x[0], x[1]] for x in zip(plot_idx[idx][1], plot_idx[idx+1][1], logg) if x[2]==4.0])
    UB45, BV45 = zip(*[[x[0], x[1]] for x in zip(plot_idx[idx][1], plot_idx[idx+1][1], logg) if x[2]==4.5])
    UB5, BV5 = zip(*[[x[0], x[1]] for x in zip(plot_idx[idx][1], plot_idx[idx+1][1], logg) if x[2]==5.0])
    p3 = axes[idx].scatter(UB3, BV3, c=colors[3.0], edgecolor="none")
    p35 = axes[idx].scatter(UB35, BV35, c=colors[3.5], edgecolor="none")
    p4 = axes[idx].scatter(UB4, BV4, c=colors[4.0], edgecolor="none")
    p45 = axes[idx].scatter(UB45, BV45, c=colors[4.5], edgecolor="none")
    p5 = axes[idx].scatter(UB5, BV5, c=colors[5.0], edgecolor="none")
    pN = axes[idx].scatter()
    if idx == 0: axes[idx].set_title("Color plots")
    #plt.gray()
  axes[-1].legend((p3,p35,p4,p45,p5), ("$log(g)=3.0$","$3.5$","$4.0$","$4.5$","$5.0$"))
  #f.subplots_adjust(hspace=0)
  #plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)
  fig = plt.gcf()
  fig.set_size_inches(12,24)
  plt.savefig('colorplot-1.png',dpi=100)"""

"""def basic_colors():
  taurus = np.genfromtxt("taurus.dat", skip_header=1, usecols=(3,7,8,9,10,11,12,1), dtype=None)
  SpT_taurus = [x[0] for x in taurus] 
  photometry = {"V": [x[1] for x in taurus], "Ic": [x[3] for x in taurus], "J":[x[4]+x[5]+x[6] for x in taurus], "H": [x[5]+x[6] for x in taurus], "Ks": [x[6] for x in taurus]}
  l = yso.yso(SpT_taurus, photometry, [140.0 for x in taurus])
  l.load_file("pecaut_mamajek.vot")
  l.load_table("table6")"""
