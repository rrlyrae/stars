src package
===========

Submodules
----------

src.WORKING_refactor_isochrones module
--------------------------------------

.. automodule:: src.WORKING_refactor_isochrones
    :members:
    :undoc-members:
    :show-inheritance:

src.config module
-----------------

.. automodule:: src.config
    :members:
    :undoc-members:
    :show-inheritance:

src.old_code module
-------------------

.. automodule:: src.old_code
    :members:
    :undoc-members:
    :show-inheritance:

src.plot module
---------------

.. automodule:: src.plot
    :members:
    :undoc-members:
    :show-inheritance:

src.plotgen module
------------------

.. automodule:: src.plotgen
    :members:
    :undoc-members:
    :show-inheritance:

src.test_taurus module
----------------------

.. automodule:: src.test_taurus
    :members:
    :undoc-members:
    :show-inheritance:

src.tlogl module
----------------

.. automodule:: src.tlogl
    :members:
    :undoc-members:
    :show-inheritance:

src.yso module
--------------

.. automodule:: src.yso
    :members:
    :undoc-members:
    :show-inheritance:

src.yso_HL_Tau module
---------------------

.. automodule:: src.yso_HL_Tau
    :members:
    :undoc-members:
    :show-inheritance:

src.yso_distr_ages module
-------------------------

.. automodule:: src.yso_distr_ages
    :members:
    :undoc-members:
    :show-inheritance:

src.yso_new_not_working module
------------------------------

.. automodule:: src.yso_new_not_working
    :members:
    :undoc-members:
    :show-inheritance:

src.yso_old module
------------------

.. automodule:: src.yso_old
    :members:
    :undoc-members:
    :show-inheritance:

src.ysot module
---------------

.. automodule:: src.ysot
    :members:
    :undoc-members:
    :show-inheritance:

src.ysot_lamori module
----------------------

.. automodule:: src.ysot_lamori
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src
    :members:
    :undoc-members:
    :show-inheritance:
