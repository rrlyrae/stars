.. yso.py documentation master file, created by
   sphinx-quickstart on Fri May 27 02:04:44 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to yso.py's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 4

   src


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

